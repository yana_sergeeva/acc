<?php
return [
    'adminEmail' => 'noreply@temposys.name',
    'supportEmail' => 'jasmintech0@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
