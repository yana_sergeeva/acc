<?php

namespace common\models;

use Yii;
use common\models\Card;
use common\models\Resource;
use common\models\UserAccount;
use common\models\Category;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "account".
 *
 * @property integer $id
 * @property string $title
 * @property integer $owner_id
 * @property integer default_currency_id
 * @property integer $status
 *
 * @property User[] $users
 */
class FinAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id'], 'required'],
            [['owner_id', 'default_currency_id', 'status'], 'integer'],
            [['title'],'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'status' => Yii::t('app', 'Status'),
            'default_currency_id' => Yii::t('app', 'Currency'),
        ];
    }

    public static function createBase()
    {
        // create Fin Account
        $acc = new self;
        $acc->title = Yii::t('ui', 'Base Account');
        $acc->owner_id = \Yii::$app->user->id;
        $acc->default_currency_id = 1;
        $acc->save();

        // Create assign to User
        $accUser = new UserAccount();
        $accUser->user_id = \Yii::$app->user->id;
        $accUser->account_id = $acc->id;
        $accUser->role = UserAccount::ROLE_ADMIN;
        $accUser->save();


        // create Cards
        $card = new Card();
        $card->account_id = $acc->id;
        $card->currency_id = $acc->default_currency_id;
        $card->title = 'Cash';
        $card->total = '0';
        $card->status = Card::STATUS_ACTIVE;
        $card->save();

        // create Incomes
        $income = new Resource();
        $income->title = 'Salary';
        $income->account_id = $acc->id;
        $income->status = Resource::STATUS_ACTIVE;
        $income->save();

        // create Categories
        $cat = new Category();
        $cat->title = 'Food';
        $cat->account_id = $acc->id;
        $cat->status = Category::STATUS_ACTIVE;
        $cat->save();

        return $acc->id;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('user_account', ['account_id' => 'id']);
    }


}
