<?php

namespace common\models;

use dektrium\user\models\Account;
use Yii;

/**
 * This is the model class for table "user_account".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id
 * @property string $role
 * @property integer $status
 */
class UserAccount extends \yii\db\ActiveRecord
{
    const ROLE_ADMIN = 'admin';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'account_id'], 'required'],
            [['user_id', 'account_id', 'status'], 'integer'],
            [['role'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public static function getFinId()
    {
        $acc_id = Yii::$app->user->identity->current_account_id;

        if($acc_id != null){
            $usAcc = UserAccount::findOne(['account_id'=>$acc_id, 'user_id'=>Yii::$app->user->id]);
            if(!empty($usAcc)) {
                Yii::$app->user->identity->setCurrentAccount($acc_id);
                return $acc_id;
            }
        }

        $acc = self::find()->where(['user_id' => Yii::$app->user->id])->one();

        if(empty($acc)) {
            $acc_id = FinAccount::createBase();
        }
        else{
            $acc_id = $acc->account_id;
        }

        Yii::$app->user->identity->setCurrentAccount($acc_id);
        return $acc_id;
    }
}
