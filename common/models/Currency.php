<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $iso_code
 * @property string $symbol
 * @property string $unicode
 * @property string $position
 * @property string $comments
 * @property integer $status
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iso_code'], 'required'],
            [['status'], 'integer'],
            [['iso_code', 'symbol', 'unicode', 'position', 'comments'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'iso_code' => Yii::t('app', 'Iso Code'),
            'symbol' => Yii::t('app', 'Symbol'),
            'unicode' => Yii::t('app', 'Unicode'),
            'position' => Yii::t('app', 'Position'),
            'comments' => Yii::t('app', 'Comments'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
