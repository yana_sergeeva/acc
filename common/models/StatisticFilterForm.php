<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 6/30/17
 * Time: 5:28 PM
 */

namespace common\models;


use Yii;
use yii\base\Model;

class StatisticFilterForm extends Model
{
    public $date;
    public $dateTo;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'dateTo'], 'safe'],
            [['date', 'dateTo'], 'date'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('app', 'Date'),
            'dateTo' => Yii::t('app', 'Date To'),
        ];
    }
}