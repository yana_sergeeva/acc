<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $icon
 * @property string $title
 * @property string $account_id
 * @property integer $status
 */
class Category extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'account_id'], 'required'],
            [['status', 'account_id'], 'integer'],
            [['icon', 'title'], 'string', 'max' => 255],
            [['is_deleted'],"boolean"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'icon' => Yii::t('app', 'Icon'),
            'title' => Yii::t('app', 'Title'),
            'account_id' => Yii::t('app', 'Account ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

//    public function beforeDelete()
//    {
//        $toCategory = Operation::find()->where(['to_type' => 'category'])->andWhere(['to_id' => $this->id])->all();
//
//        foreach ($toCategory as $category){
//            $category->to_id = null;
//            $category->save(false);
//        }
//
//        return true;
//    }
}
