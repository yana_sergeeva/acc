<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Operation;
use yii\db\Expression;

/**
 * OperationSearch represents the model behind the search form about `common\models\Operation`.
 */
class OperationSearch extends Operation
{
    public $date_from = '';
    public $date_to = '';

    public $from_key = '';

    public $card_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'account_id', 'from_id', 'to_id', 'user_id', 'status', 'card_id'], 'integer'],
            [['from_key', 'from_type', 'to_type', 'operation', 'comments', 'updated_at', 'date_from', 'date_to'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $is_default)
    {
        $query = Operation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'account_id' => $this->account_id
        ]);

        if(!empty($this->operation))
        {
            $query->andWhere(['like', 'operation', $this->operation]);
        }

        if (!empty($this->to_id) && !empty($this->from_id) && !empty($this->card_id))
        {
            $query->andWhere([
                'OR',
                new Expression("(to_type='category' AND to_id={$this->to_id})"),
                new Expression("(from_type='resource' AND from_id={$this->from_id})"),
                new Expression("(to_type='card' AND to_id={$this->card_id})"),
                new Expression("(from_type='card' AND from_id={$this->card_id})")
            ]);
        }
        elseif (!empty($this->to_id) && !empty($this->from_id))
        {
            $query->andWhere([
                'OR',
                new Expression("(to_type='category' AND to_id={$this->to_id})"),
                new Expression("(from_type='resource' AND from_id={$this->from_id})"),
            ]);
        }
        elseif (!empty($this->from_id) && !empty($this->card_id))
        {
            $query->andWhere([
                'AND',
                new Expression("(from_type='resource' AND from_id={$this->from_id})"),
                new Expression("(to_type='card' AND to_id={$this->card_id})"),
            ]);
        }
        elseif (!empty($this->to_id) && !empty($this->card_id)){
            $query->andWhere([
                'AND',
                new Expression("(to_type='category' AND to_id={$this->to_id})"),
                new Expression("(from_type='card' AND from_id={$this->card_id})")
            ]);
        }
        else{
            if(!empty($this->to_id)){
                $query->andWhere(['to_type'=>'category', 'to_id'=>$this->to_id]);
            }
            if(!empty($this->from_id)){
                $query->andWhere(['from_type'=>'resource', 'from_id'=>$this->from_id]);
            }
            if(!empty($this->card_id)){
                $query->andWhere([
                    'OR',
                    new Expression("(to_type='card' AND to_id={$this->card_id})"),
                    new Expression("(from_type='card' AND from_id={$this->card_id})")
                ]);
            }
        }

        if (!empty($this->date_from)) {
            $query->andWhere("updated_at >= :date_from", [':date_from' => $this->date_from]);
        }

        if (!empty($this->date_to)) {
            $query->andWhere("updated_at <= :date_to", [':date_to' => $this->date_to." 23:59:59"]);
        }

        $query->andFilterWhere(['like', 'comments', $this->comments]);

        if($is_default)
            $query->orderBy(['updated_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
