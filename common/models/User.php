<?php

namespace common\models;

use dektrium\user\models\Account;
use dektrium\user\models\Profile;
use dektrium\user\models\Token;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property integer $current_account_id
 *
 * @property Profile $profile
 * @property FinAccount $currentAccount
 * @property FinAccount[] $accounts
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class User extends \dektrium\user\models\User
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'current_account_id'], 'integer'],
            [['username', 'email', 'unconfirmed_email'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 45],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'confirmed_at' => Yii::t('app', 'Confirmed At'),
            'unconfirmed_email' => Yii::t('app', 'Unconfirmed Email'),
            'blocked_at' => Yii::t('app', 'Blocked At'),
            'registration_ip' => Yii::t('app', 'Registration Ip'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'flags' => Yii::t('app', 'Flags'),
            'current_account_id' => Yii::t('app', 'Current Account'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }

    public function isOwner()
    {
        $accountId = UserAccount::getFinId();
        $model = FinAccount::findOne($accountId);
        if(!empty($model) && $this->id == $model->owner_id){
            return true;
        }
        return false;
    }

    /**
     * @return string
     */

    public function attemptConfirmation($code)
    {
        $token = $this->finder->findTokenByParams($this->id, $code, Token::TYPE_CONFIRMATION);

        if ($token instanceof Token && !$token->isExpired) {
            if (($success = $this->confirm())) {
                $token->delete();
                \Yii::$app->user->login($this, $this->module->rememberFor);
                $message = \Yii::t('user', 'Thank you, registration is now complete.');
            } else {
                $message = \Yii::t('user', 'Something went wrong and your account has not been confirmed.');
            }
        } else {
            $success = false;
            $message = \Yii::t('message', 'The confirmation link is invalid or expired.') . ' <a href="http://acc.jdev.com.ua/user/resend">'.Yii::t('message','Please try requesting a new one.').'</a>';
        }

        \Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);

        return $success;
    }


    public function getRole($accountId)
    {
        $ua = UserAccount::findOne(['user_id'=>$this->id, 'account_id'=>$accountId]);
        return empty($ua)?'':$ua->role;
    }

    public function getCurrentAccount()
    {
        return $this->hasOne(FinAccount::className(), ['id' => 'current_account_id']);
    }

    public function getAccounts()
    {
        return $this->hasMany(FinAccount::className(), ['id' => 'account_id'])->viaTable('user_account',['user_id'=>'id']);
    }

    public function setCurrentAccount($accountId){
        $this->current_account_id = $accountId;
        $this->save();
    }
}
