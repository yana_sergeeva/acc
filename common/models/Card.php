<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "card".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $title
 * @property integer $currency_id
 * @property double $total
 * @property string $updated_at
 * @property integer $status
 * @property integer $display
 */
class Card extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISACTIVE = 0;

    const CREDIT_CARD =  "Credit";
    const ORDINARY_CARD =  "Debit";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'title', 'currency_id'], 'required'],
            [['account_id', 'currency_id', 'display', 'status'], 'integer'],
            [['total'], 'number'],
            [['updated_at'], 'safe'],
            [['type'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['is_deleted'],"boolean"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'title' => Yii::t('app', 'Title'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'total' => Yii::t('app', 'Total'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

//    public function beforeDelete()
//    {
//        $toCard = Operation::find()->where(['to_type' => 'card'])->andWhere(['to_id' => $this->id])->all();
//        foreach ($toCard as $card){
//            $card->to_id = null;
//            $card->save(false);
//        }
//
//        $fromCard = Operation::find()->where(['from_type' => 'card'])->andWhere(['from_id' => $this->id])->all();
//        foreach ($fromCard as $card){
//            $card->from_id = null;
//            $card->save(false);
//        }
//
//        return true;
//    }
}
