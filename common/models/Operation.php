<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "operation".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $from_type
 * @property integer $from_id
 * @property string $to_type
 * @property integer $to_id
 * @property string $operation
 * @property double $amount
 * @property string $comments
 * @property integer $user_id
 * @property string $updated_at
 * @property integer $status
 */
class Operation extends \yii\db\ActiveRecord
{
    const OPERATOR_IN = 'in';
    const OPERATOR_OUT = 'out';
    const OPERATOR_BETWEEN = 'btw';

    public $date_from = '';
    public $date_to = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'from_type', 'from_id', 'to_type', 'to_id', 'user_id'], 'required'],
            [['account_id', 'from_id', 'to_id', 'user_id', 'status'], 'integer'],
            [['amount'], 'number'],
            [['updated_at', 'date_from', 'date_to'], 'safe'],
            [['from_type', 'to_type', 'operation', 'comments'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'from_type' => Yii::t('app', 'From Type'),
            'from_id' => Yii::t('app', 'From ID'),
            'to_type' => Yii::t('app', 'To Type'),
            'to_id' => Yii::t('app', 'To ID'),
            'operation' => Yii::t('app', 'Operation'),
            'amount' => Yii::t('app', 'Amount'),
            'comments' => Yii::t('app', 'Comment'),
            'user_id' => Yii::t('app', 'User'),
            'updated_at' => Yii::t('app', 'Date'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
