<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "resource".
 *
 * @property integer $id
 * @property string $title
 * @property string $account_id
 * @property integer $status
 */
class Resource extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resource';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'account_id'], 'required'],
            [['status', 'account_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['is_deleted'],"boolean"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'account_id' => Yii::t('app', 'Account ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

//    public function beforeDelete()
//    {
//        $fromResource = Operation::find()->where(['from_type' => 'resource'])->andWhere(['from_id' => $this->id])->all();
//        foreach ($fromResource as $resource){
//            $resource->from_id = null;
//            $resource->save(false);
//        }
//        return true;
//    }
}
