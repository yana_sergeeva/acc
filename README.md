Accountant service
===============================

Installation:

```
composer update
php init
php yii migrate/up --migrationPath=@vendor/lajax/yii2-translate-manager/migrations
php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
php yii migrate --migrationPath=@yii/rbac/migrations
php yii migrate
php yii rbac/init
```
