<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Users', 'icon' => 'users', 'url' => ['/user/admin/index'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Login', 'icon' => 'sign-in', 'url' => ['/user/security/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
