<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
if (!Yii::$app->user->isGuest) {
    $profile = Yii::$app->user->identity->profile;
}
?>

<header class="main-header">

<?= Html::a('<span class="logo-mini">Aсс</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">


                <!-- User Account: style can be found in dropdown.less -->
                <?php if (!Yii::$app->user->isGuest): ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user fa-fw"></i>
                            <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Menu Body -->
                            <li class="user-header">

                                <?=
                                Html::img($profile->getAvatarUrl(230), [
                                    'class' => 'img-circle',
                                    'alt' => $profile->user->username,
                                ])
                                ?>

                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small>
                                        <?php if (!empty($profile->public_email)): ?>
                                            <i class="glyphicon glyphicon-envelope"></i> <?= Html::a(Html::encode($profile->public_email), 'mailto:' . Html::encode($profile->public_email)) ?>
                                        <?php endif; ?><br/>
                                        <i class="glyphicon glyphicon-time"></i> Created at <?= date('d-m-Y', $profile->user->created_at) ?>
                                    </small>
                                </p>
                            </li>
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <a href="<?= \yii\helpers\Url::to(['/user/settings/profile']) ?>" >Profile</a>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <a href="<?= \yii\helpers\Url::to(['/user/settings/account']) ?>" >Account</a>
                                    </div>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">

                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                            'Sign out', ['/user/security/logout'], [
                                        'data-method' => 'post',
                                        'class' => 'btn btn-default btn-flat'
                                            ]
                                    )
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>

            </ul>
        </div>
    </nav>
</header>
