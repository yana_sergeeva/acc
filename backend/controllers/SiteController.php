<?php
namespace backend\controllers;

use dektrium\user\models\Token;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'switch'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect('/user/admin/index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
        return $this->goHome();
    }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {


            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @param null $id
     * @return \yii\web\Response
     */
    public function actionSwitch($id = null)
    {
        $token = new Token();
        $token->user_id = $id;
        $token->type = 6;
        $token->code = Yii::$app->security->generateRandomString();
        $token->save();

        return $this->redirect(Yii::$app->urlManagerFrontend->createUrl(['/site/login-by-token', 'code' => $token->code]));
    }
}
