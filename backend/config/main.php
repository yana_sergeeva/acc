<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name'     => 'Admin.Accountant',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language'            => 'en',
    'components' => [

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views/security' => '@app/views/site',
                    '@dektrium/user/views/recovery' => '@app/views/site',
                    '@dektrium/user/views/admin' => '@app/views/site',
                    '@dektrium/user/views/profile' => '@app/views/site',
                    '@dektrium/user/views/' => '@app/views/site',
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                // here is the list of clients you want to use
                // you can read more in the "Available clients" section
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '1070011459760108',
                    'clientSecret' => 'dc8418296d744851e543d31db09033ad',
                ],
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => '178236982110-a777fu4mtkm0gu6og2t90vn82m6g9sp4.apps.googleusercontent.com',
                    'clientSecret' => '1Af_wzAvuE6RmzwHQnWFiwe4',
                ],
            ],
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'           => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => false,
            'rules'               => [
                '/'                                      => 'site/index',
                '<action:[\w\-]+>'                           => 'site/<action>',
                'statistic/index/<year:\d+>/<month:\d+>' => 'statistic/index',

                '<controller:[\w\-]+>/<id:\d+>'              => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>'          => '<controller>/<action>',
            ]
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'adminPermission' => 'admin',
            'enableUnconfirmedLogin' => true,
            'enableGeneratingPassword' => true,
            'enableFlashMessages' => false,
            'confirmWithin' => 21600,
            'cost' => 12,      
        ],

    ],
    'params' => $params,
];
