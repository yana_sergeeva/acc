<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=acc',
            'username' => 'acc',
            'password' => 'acc',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mailtrap.io',
                'username' => '6ad1e4408f2f84',
                'password' => 'd33413d5853431',
                'port' => '2525',
                'encryption' => 'tls',
            ],
        ],
    ],
];
