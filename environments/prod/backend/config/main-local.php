<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules'               => [
                'lang' => 'translatemanager/language/list',

                '/'                                      => 'site/index',
                '<action:[\w\-]+>'                           => 'site/<action>',
                'statistic/index/<year:\d+>/<month:\d+>' => 'statistic/index',

                '<controller:[\w\-]+>/<id:\d+>'              => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>'          => '<controller>/<action>',
            ],
            'baseUrl' => 'http://acc.temposys.name',
        ],
    ],
];
