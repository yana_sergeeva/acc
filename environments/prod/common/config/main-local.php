<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=acc',
            'username' => 'acc_usr',
            'password' => '4zAzHhE47nc3rE64',
            'charset' => 'utf8',
            'tablePrefix' => '',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
