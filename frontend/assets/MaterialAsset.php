<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class MaterialAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'materialize/css/themes/collapsible-menu/materialize.css',
        'materialize/css/themes/collapsible-menu/style.css',
        'materialize/css/custom/custom.css',
        'materialize/vendors/prism/prism.css',
        'materialize/vendors/perfect-scrollbar/perfect-scrollbar.css',
        'materialize/vendors/flag-icon/css/flag-icon.min.css',
        'css/style.css'
    ];
    public $js = [
        'materialize/js/materialize.js',
        'materialize/vendors/prism/prism.js',
        'materialize/vendors/perfect-scrollbar/perfect-scrollbar.min.js',
        'materialize/js/plugins.js',
        'materialize/js/custom-script.js',
        'materialize/js/scripts/advanced-ui-modals.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}