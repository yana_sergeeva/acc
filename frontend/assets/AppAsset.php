<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bower_components/metisMenu/dist/metisMenu.min.css',
//        'css/sb-admin-2.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
        //'css/style.css'
    ];
    public $js = [
        //'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/metisMenu/dist/metisMenu.min.js',
//        'js/sb-admin-2.js',
        'js/main.js',
        //'materialize/js/materialize.min.js',
        //'materialize/vendors/prism/prism.js',
        //'materialize/vendors/perfect-scrollbar/perfect-scrollbar.min.js',
        //'materialize/js/plugins.js',
        //'materialize/js/custom-script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
