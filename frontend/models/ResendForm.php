<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.11.17
 * Time: 17:13
 */

namespace frontend\models;


use common\models\User;
use dektrium\user\models\Token;

class ResendForm  extends \dektrium\user\models\ResendForm
{
    public function resend()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->finder->findUserByEmail($this->email);

        if ($user instanceof User && !$user->isConfirmed) {
            /** @var Token $token */
            $token = \Yii::createObject([
                'class' => Token::className(),
                'user_id' => $user->id,
                'type' => Token::TYPE_CONFIRMATION,
            ]);
            $token->save(false);
            $this->mailer->sendConfirmationMessage($user, $token);
            \Yii::$app->session->setFlash(
                'info',
                \Yii::t(
                    'user',
                    'A message has been sent to your email address. It contains a confirmation link that you must click to complete registration.'
                )
            );
        }else{
            \Yii::$app->session->setFlash(
                'danger',
                \Yii::t(
                    'message',
                    'Account already exists or some error occured. If your account isn\'t confirmed please contact us!'
                )
            );
        }
        return true;
    }
}