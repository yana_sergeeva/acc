<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{

    public $name;
    public $email;
    public $subject;
    public $body;
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Lf1XEQUAAAAAHGy0jSyOeJm0XklPJzokvgJgLXl', 'uncheckedMessage' => 'Please confirm that you are not a bot.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'       => Yii::t('app', 'Name'),
            'email'      => Yii::t('app', 'Email'),
            'subject'    => Yii::t('app', 'Subject'),
            'body'       => Yii::t('app', 'Body'),
            'verifyCode' => Yii::t('app', 'Verification Code'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        $body = $this->name . "<" . $this->email . ">\n" . $this->body;
        return Yii::$app->mailer->compose()
                        ->setTo(Yii::$app->params['supportEmail'])
                        ->setFrom($email)
                        ->setSubject($this->subject)
                        ->setTextBody($body)
                        ->send();
    }
}
