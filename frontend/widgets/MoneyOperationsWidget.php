<?php
namespace frontend\widgets;

use common\models\Card;
use common\models\Category;
use common\models\Currency;
use common\models\Operation;
use common\models\Resource;
use common\models\UserAccount;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: dev
 * Date: 9/9/17
 * Time: 9:19 AM
 */
class MoneyOperationsWidget extends Widget
{

    public $operation;
    public $categories;
    public $resources;
    public $cards;
    public $hash;

    public function init()
    {
        $this->operation = new Operation();
        $this->hash = '';
        $accountId = UserAccount::getFinId();

        $this->cards = $this->getCards($accountId);
        $this->categories = $this->getCategories($accountId);
        $this->resources = $this->getResources($accountId);

    }

    public function run()
    {
        return $this->render('money-operations-widget',[
            'operation' => $this->operation,
            'categories'=>$this->categories,
            'resources'=>$this->resources,
            'cards' => $this->cards,
            'hash'=>$this->hash,
        ]);
    }

    private function getCards($accountId)
    {
        $cardRes = Card::find()->where(['account_id' => $accountId])->andWhere(['is_deleted' => false])
            ->orderBy(['display' => SORT_ASC, 'total' => SORT_DESC])->all();
        $cards = [0 => Yii::t('ui', '-- choose --')];
        $money = [];
        foreach($cardRes as $cardItem) {
            $cur = Currency::find()->where(['id' => $cardItem->currency_id])->one();

            $cards[$cardItem->id] = $cardItem->title  . " (" . number_format($cardItem->total, 2, '.', ' ').$cur->symbol . ")";
            $money[$cardItem->id]=$cardItem->total;
        }

        $JavaScript[] = "var money = JSON.parse('" . Json::encode($money) . "');";
        $view = $this->getView();
        $view->registerJs(implode(" \n", $JavaScript), View::POS_END);


        return $cards;
    }

    private function getCategories($accountId){
        return ArrayHelper::merge(
            [0 => Yii::t('ui', '-- choose --')],
            ArrayHelper::map(
                Category::find()->where([
                    'account_id' => $accountId,
                    'status' => Category::STATUS_ACTIVE,
                    'is_deleted' => false])->all(),
                'id', 'title')
        );
    }

    private function getResources($accountId){
        return ArrayHelper::merge(
            [0 => Yii::t('ui', '-- choose --')],
            ArrayHelper::map(Resource::find()->where([
                'account_id' => $accountId,
                'status' => Resource::STATUS_ACTIVE,
                'is_deleted' => false])->all(),
                'id', 'title')
        );
    }
}