<?php

namespace frontend\widgets;

use lajax\translatemanager\models\Language;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class CustomLanguagePicker extends \lajax\languagepicker\widgets\LanguagePicker
{
    /**
     * Rendering languege element.
     * @param string $language The property of a given language.
     * @param string $name The property of a language name.
     * @param string $template The basic structure of a language element of the displayed language picker
     * Elements to replace: "{link}" URL to call when changing language.
     *  "{name}" name corresponding to a language element, e.g.: English
     *  "{language}" unique identifier of the language element. e.g.: en, en-US
     * @return string the rendered result
     */
    protected function renderItem($language, $name, $template)
    {

        if ($this->encodeLabels) {
            $language = Html::encode($language);
            $name = Html::encode($name);
        }

        $params = array_merge([''], Yii::$app->request->queryParams, ['language-picker-language' => $language]);
        $languageModel = Language::find()->where(['like', 'name', $name])->one();

        return strtr($template, [
            '{link}' => Url::to($params),
            '{name}' => $name,
            '{language}' => $language,
            '{country}' => $languageModel->country ?? '',
        ]);
    }
}