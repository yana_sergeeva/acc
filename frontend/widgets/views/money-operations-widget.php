<?php

namespace frontend\widgets;

use common\models\Operation;
use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**@var $hash string*/
/**@var $operation Operation*/
/**@var $cards array*/
/**@var $categories array*/
/**@var $resources array*/
$this->registerJsFile( '/js/confirm-modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);

?>
<div class="hidden">
    <div id="message-1"><?=Yii::t('app','If you continue the operation, you will have a negative balance!')?></div>
    <div id="message-2"><?=Yii::t('app','You have a negative balance!')?></div>
</div>

<div style="position:relative;" class="col s12">
    <div class="row">
        <div style="padding: 1px;" class="col s12">
            <ul  class="tabs tab-demo-active z-depth-1">
                <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="#sapien1"><i style="margin: 0; float: none" class="material-icons">arrow_upward</i></a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light" href="#activeone1"><i style="margin: 0; float: none"  class="material-icons">arrow_downward</i></a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light" href="#vestibulum1"><i style="margin: 0; float: none"  class="material-icons">compare_arrows</i></a>
                </li>
                <li class="indicator" style="right: 777px; left: 388px;"></li></ul>
        </div>

        <div class="col s12">
            <div id="sapien1" class="col s12" style="">
                <h4 style="font-size: 16px;" class="control-sidebar-heading"><?= Yii::t('ui', 'Add Expenses') ?></h4>
                <?php $formOut = ActiveForm::begin([
                    //'action'=>['operation/add-operation'],
                    //'method' => 'POST'
                ]); ?>
                <?= Html::hiddenInput('Operation[from_type]', 'card'); ?>
                <?= Html::hiddenInput('Operation[to_type]', 'category'); ?>
                <?= Html::hiddenInput('Operation[operation]', 'out'); ?>

                <?= $formOut->field($operation, 'from_id')->dropDownList($cards, ['id'=>'in_from_id', 'class' => ''])->label(Yii::t('ui', 'From')) ?>
                <?= $formOut->field($operation, 'to_id')->dropDownList($categories,['class' => ''])->label(Yii::t('ui', 'To Category')) ?>
                <?= $formOut->field($operation, 'amount')->textInput(['onkeyup'=>"return floatAmount(this);", 'id'=>'in_amount']) ?>
                <?= $formOut->field($operation, 'comments')->textarea() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('ui', 'Add'), ['class' => 'btn btn-primary btn-ajax', 'data-confirm'=>'add_expenses']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div id="activeone1" class="col s12 " style="display: none;">
                <h4 style="font-size: 16px;" class="control-sidebar-heading"><?= Yii::t('ui', 'Money Income') ?></h4>
                <?php $formIn = ActiveForm::begin([
                    //'action'=>['operation/add-operation']
                ]); ?>
                <?= Html::hiddenInput('Operation[from_type]', 'resource'); ?>
                <?= Html::hiddenInput('Operation[to_type]', 'card'); ?>
                <?= Html::hiddenInput('Operation[operation]', 'in'); ?>

                <?= $formIn->field($operation, 'from_id')->dropDownList($resources,['class' => 'input-field'])->label(Yii::t('ui', 'From')) ?>
                <?= $formIn->field($operation, 'to_id')->dropDownList($cards,['class' => 'input-field'])->label(Yii::t('ui', 'To Card')) ?>
                <?= $formIn->field($operation, 'amount')->textInput(['onkeyup'=>"return floatAmount(this);"]) ?>
                <?= $formIn->field($operation, 'comments')->textarea() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('ui', 'Add'), ['class' => 'btn btn-primary btn-ajax']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div id="vestibulum1" class="col s12 " style="display: none;">
                <h4 style="font-size: 16px;" class="control-sidebar-heading"><?= Yii::t('ui', 'Move Money Between Cards') ?></h4>
                    <?php $formBtw = ActiveForm::begin([
                        'id' => 'cardForm',
                        //'action'=>['operation/add-operation']
                    ]); ?>
                        <?= Html::hiddenInput('Operation[from_type]', 'card'); ?>
                        <?= Html::hiddenInput('Operation[to_type]', 'card'); ?>
                        <?= Html::hiddenInput('Operation[operation]', 'btw'); ?>

                        <?= $formBtw->field($operation, 'from_id')->dropDownList($cards, ['id'=>'btw_from_id','class' => 'input-field'])->label(Yii::t('ui', 'From Card')) ?>
                        <?= $formBtw->field($operation, 'to_id')->dropDownList($cards,['id'=>'btw_to_id','class' => 'input-field'])->label(Yii::t('ui', 'To Card')) ?>
                        <?= $formBtw->field($operation, 'amount')->textInput(['onkeyup'=>"return floatAmount(this);", 'id'=>'btw_amount']) ?>
                        <?= $formBtw->field($operation, 'comments')->textarea() ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('ui', 'Move'), ['class' => 'btn btn-primary btn-ajax', 'data-confirm'=>'add_move']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>



