<?php

namespace frontend\controllers;

use common\models\Resource;
use common\models\StatisticFilterForm;
use Faker\Provider\DateTime;
use Yii;
use yii\web\Controller;
use common\models\UserAccount;
use common\models\FinAccount;
use common\models\Currency;
use yii\helpers\ArrayHelper;
use common\models\Category;
use common\models\Operation;
use yii\web\BadRequestHttpException;

class StatisticController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }


        $filterForm = new StatisticFilterForm();
        if($filterForm->load(Yii::$app->request->post())){

            if($filterForm->date)
            {
                $date = "01.".$filterForm->date;
                $year = date('Y',strtotime($date));
                $month = date('m',strtotime($date));
            }
            else
            {
                $year = date('Y');
                $month=date('m');
                $filterForm->date = $month.".".$year;
            }


            if($filterForm->dateTo)
            {
                $date1 = "01.".$filterForm->dateTo;
                $year1 = date('Y',strtotime($date1));
                $month1 = date('m',strtotime($date1));
            }
            else
            {
                $year1 = date('Y');
                $month1 =date('m');
                $filterForm->dateTo = $month.".".$year;
            }
        }else{
            $year = date('Y');
            $month=date('m');
            $filterForm->date = $month.".".$year;

            $year1 = date('Y');
            $month1=date('m');
            $filterForm->dateTo = $month.".".$year;
        }


        $accountId = UserAccount::getFinId();
        $accData = FinAccount::findOne(['id' => $accountId]);

        if ($accData == NULL) {
            throw new BadRequestHttpException('Bad request data!');
        }
        $cur = Currency::findOne(['id' => $accData->default_currency_id]);
        $categories = ArrayHelper::merge([0 => Yii::t('ui', '-- choose --')],
            ArrayHelper::map(Category::find()->where(['account_id' => $accountId, 'status' => Category::STATUS_ACTIVE])->all(), 'id', 'title'));

        $resources = Resource::find()->where(['account_id' => $accountId])->all();

        // spent
        $spent = Operation::find()->where(['account_id' => $accountId, 'operation' => 'out'])->andFilterWhere(['like', 'updated_at', $year."-".$month]);
        $earned = Operation::find()->where(['account_id' => $accountId, 'operation' => 'in'])->andFilterWhere(['like', 'updated_at', $year1."-".$month1]);

        // category statistic
        $categoryStatistic = [];
        $categoryStatisticEarned = [];

        $sumArray = [];
        $nameArray = [];
        $sumArrayEarned = [];
        $nameArrayEarned = [];


        $colorArray = array("#ff5050", "#3399ff", "#99ff66", "#ffff66", "#ff9933", "#cc33ff" , "Black");
        $colorArray1 = [];
        $i = 0;

        foreach ($categories as $key=>$title)
        {
            $sum = Operation::find()->where(['account_id' => $accountId, 'operation' => 'out', 'to_type' => 'category', 'to_id' => $key])
                ->andFilterWhere(['like', 'updated_at', $year."-".$month])->sum('amount');

            if (abs($sum) > 0) {
                $sumArray[] = abs($sum);
                $nameArray[] = $title;
                $colorArray1[] = $colorArray[$i];
                $i++;
                if($i == count($colorArray))
                    $i = 0;
//                $categoryStatistic[$title] = number_format(abs($sum), 2, '.', '&nbsp;');
                $categoryStatistic[$key] = ['title' => $title, 'amount' => number_format(abs($sum), 2, '.', '&nbsp;').$cur->symbol];
            }

        }

        foreach ($resources as $key=>$title)
        {
            $sumEarned = Operation::find()->where(['account_id' => $accountId, 'operation' => 'in', 'from_type' => 'resource','to_type' => 'card' , 'from_id' => $title->id])
                ->andFilterWhere(['like', 'updated_at', $year1."-".$month1])->sum('amount');

            if (abs($sumEarned) > 0) {
                $sumArrayEarned[] = abs($sumEarned);
                $nameArrayEarned[] = $title->title;
                $colorArray1[] = $colorArray[$i];
                $i++;
                if($i == count($colorArray))
                    $i = 0;
//                $categoryStatistic[$title] = number_format(abs($sum), 2, '.', '&nbsp;');
                $categoryStatisticEarned[$key] = ['title' => $title->title, 'amount' => number_format(abs($sumEarned), 2, '.', '&nbsp;').$cur->symbol];
            }
        }

        $sum = $sumArray;
        $titles = $nameArray;

        //dg($filterForm);



        return $this->render('index', [
            'spent' => number_format(abs ($spent->sum('amount')), 0, '.', ' '),
            'earned' => number_format(abs ($earned->sum('amount')), 0, '.', ' '),
            'categoryStatistic' => $categoryStatistic,
            'categoryStatisticEarned' => $categoryStatisticEarned,

            'default_currency' => $cur->symbol,
            'filterForm' =>$filterForm,

            'sum' => $sum,
            'titles' => $titles,
            'sumEarned' => $sumArrayEarned,
            'titlesEarned' => $nameArrayEarned,


            'colors' => $colorArray1
        ]);
    }



}
