<?php
namespace frontend\controllers;

use common\models\User;
use dektrium\user\models\Token;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Operation;
use common\models\Card;
use common\models\Category;
use common\models\UserAccount;
use common\models\FinAccount;
use yii\helpers\ArrayHelper;
use common\models\Resource;
use common\models\Currency;
use yii\web\View;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','config'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login-by-token'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','config'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLoginByToken($code)
    {
        $t = Token::findOne(['code' => $code]);
        if ($t){
            $user= User::findOne($t->user_id);
            Yii::$app->user->login($user, 3600 * 24 * 30);
            $t->delete();
        }
        return $this->redirect(['/']);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/site/dashboard']);
        }
        return $this->render('index');
    }

    public function actionDashboard()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
//        var_dump(Yii::$app->user->acc);
        $accountId = UserAccount::getFinId();
        $accData = FinAccount::findOne(['id' => $accountId]);

        $categories = ArrayHelper::merge(
            [0 => Yii::t('ui', '-- choose --')],
            ArrayHelper::map(
                Category::find()->where([
                    'account_id' => $accountId,
                    'status' => Category::STATUS_ACTIVE,
                    'is_deleted' => false])->all(),

                    'id', 'title')
        );

        $cardRes = Card::find()->where(['account_id' => $accountId])->andWhere(['is_deleted' => false])
            ->orderBy(['display' => SORT_ASC, 'total' => SORT_DESC])->all();
        $cardList = [];
        $money = [];
        foreach($cardRes as $cardItem) {
            $cur = Currency::find()->where(['id' => $cardItem->currency_id])->one();
            if ($cardItem->display == 1) {
                $cardList[$cardItem->id]['title'] = $cardItem->title;
                $cardList[$cardItem->id]['total'] = number_format($cardItem->total, 2, '.', ' ').$cur->symbol;
            }
            $money[$cardItem->id]=$cardItem->total;
        }



        // Operation History
        $history = Operation::find()->where(['account_id' => $accountId])->andFilterWhere(['like', 'updated_at', date("Y-m-d")])->orderBy(['id'=>SORT_DESC])->all();

        // received
        $recieved = Operation::find()->where(['account_id' => $accountId, 'operation' => 'in'])->andFilterWhere(['like', 'updated_at', date("Y-m")]);

        // spent
        $spent = Operation::find()->where(['account_id' => $accountId, 'operation' => 'out'])->andFilterWhere(['like', 'updated_at', date("Y-m")]);

        // displayed balance
        $balance = Card::find()->where(['account_id' => $accountId, 'display' => 1, 'currency_id' => $accData->default_currency_id, 'is_deleted' => false]);
        $cur = Currency::findOne(['id' => $accData->default_currency_id]);

        // category statistic
        $categoryStatistic = [];
        foreach ($categories as $key=>$title)
        {
            $sum = Operation::find()->where(['account_id' => $accountId, 'operation' => 'out', 'to_type' => 'category', 'to_id' => $key])
                    ->andFilterWhere(['like', 'updated_at', date("Y-m")])->sum('amount');
            if (abs($sum) > 0) {
//                $categoryStatistic[$title] = number_format(abs($sum), 2, '.', '&nbsp;');
                $categoryStatistic[$key] = ['title' => $title, 'amount' => number_format(abs($sum), 2, '.', '&nbsp;').$cur->symbol];
            }
        }
//        var_dump( Operation::find()->where(['account_id' => $accountId, 'operation' => 'out', 'to_type' => 'category'])->andFilterWhere(['like', 'updated_at', date("Y-m")])->distinct('to_id')->sum('amount'));
//        var_dump($categoryStatistic);
        $open = 'yes';

        if(Yii::$app->request->isAjax){
            $open = 'no';
        }

        return $this->render('dashboard', [
            'history' => $history,
            'cardList' => $cardList,
            'categoryStatistic' => $categoryStatistic,
            'received' =>number_format($recieved->sum('amount'), 0, '.', ' '),
            'spent' => number_format(abs ($spent->sum('amount')), 0, '.', ' '),
            'balance' => number_format($balance->sum('total'), 0 , '.', ' '),
            'default_currency' => $cur->symbol,
            'open' => $open
        ]);


    }


    public function actionNewBalance(){
        $accountId = UserAccount::getFinId();
        $accData = FinAccount::findOne(['id' => $accountId]);

        $recieved = Operation::find()->where(['account_id' => $accountId, 'operation' => 'in'])->andFilterWhere(['like', 'updated_at', date("Y-m")]);

        // spent
        $spent = Operation::find()->where(['account_id' => $accountId, 'operation' => 'out'])->andFilterWhere(['like', 'updated_at', date("Y-m")]);

        // displayed balance
        $balance = Card::find()->where(['account_id' => $accountId, 'display' => 1, 'currency_id' => $accData->default_currency_id, 'is_deleted' => false]);


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'received' => number_format($recieved->sum('amount'), 0, '.', ' '),
            'spent' => number_format(abs ($spent->sum('amount')), 0, '.', ' '),
            'balance' => number_format($balance->sum('total'), 0 , '.', ' ')
        ];
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            UserAccount::getFinId();

            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $model = new ContactForm();
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }
        }
        if (!Yii::$app->user->isGuest) {
            $user = \dektrium\user\models\User::findOne(['id' => \Yii::$app->user->id]);
            $model->email = $user->email;
        }
//            return $this->refresh();
//        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
//        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionConfig()
    {
        $accountId = UserAccount::getFinId();
        $model = FinAccount::findOne($accountId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Card::updateAll(['currency_id'=>$model->default_currency_id], ['account_id' => $accountId]);
            Operation::updateAll(['currency_id'=>$model->default_currency_id], ['account_id' => $accountId]);
            return $this->redirect(['/site/index']);
        }

        $usersDataProvider = new ActiveDataProvider([
            'query'=>$model->getUsers()
        ]);

        return $this->render('config', [
            'model' => $model,
            'currencies' => ArrayHelper::map(Currency::findAll(['status' => 1]), 'id', 'iso_code'),
            'usersDataProvider' => $usersDataProvider,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


}
