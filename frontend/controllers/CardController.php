<?php

namespace frontend\controllers;

use Yii;
use common\models\Card;
use common\models\CardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UserAccount;
use yii\helpers\ArrayHelper;
use \common\models\Currency;
use common\models\FinAccount;

/**
 * CardController implements the CRUD actions for Card model.
 */
class CardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

//    public function beforeAction($action)
//    {
//        if (Yii::$app->user->isGuest) {
//            return $this->redirect(['/user/login'])->send();
//        } else {
//            parent::beforeAction($action);
//        }
//    }

    /**
     * Lists all Card models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $searchModel = new CardSearch();
        $accountId = UserAccount::getFinId();
        $request = ArrayHelper::merge(Yii::$app->request->queryParams, ['CardSearch' => ['account_id' => $accountId,'is_deleted'=> false]]);
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Card model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Card model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $accountId = UserAccount::getFinId();
        $accData = FinAccount::findOne(['id' => $accountId]);

        $model = new Card();
        $model->account_id = $accountId;
        $model->status = 1;
        $model->currency_id = $accData->default_currency_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'currencies' => ArrayHelper::map(Currency::find()->where(['status' => 1])->all(), 'id', 'iso_code'),
            ]);
        }
    }

    /**
     * Updates an existing Card model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'currencies' => ArrayHelper::map(Currency::find()->where(['status' => 1])->all(), 'id', 'iso_code'),
            ]);
        }
    }

    /**
     * Deletes an existing Card model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_deleted = true;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Card model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Card the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Card::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteMultiple(){
        if(isset($_REQUEST['selection'])){
            foreach (Yii::$app->request->post('selection') as $select){
                $card = Card::findOne($select);
                $card->is_deleted = true;
                $card->save();
            }
            Yii::$app->session->setFlash('success', "Cards were successfully removed!");
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
