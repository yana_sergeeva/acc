<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 7/4/17
 * Time: 2:32 PM
 */

namespace frontend\controllers;


use common\models\Currency;
use common\models\FinAccount;
use common\models\UserAccount;
use dektrium\user\helpers\Password;
use dektrium\user\models\RegistrationForm;
use dektrium\user\models\Token;
use dektrium\user\models\User;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class AccountController extends Controller
{
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','change-account','create','update','delete','add-user','delete-user', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Card models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = FinAccount::find()->where(
            ['IN', 'id',
                ArrayHelper::map(
                    UserAccount::findAll(['user_id'=>Yii::$app->user->id]),
                        'account_id',
                        'account_id')
            ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $currentAccountId = UserAccount::getFinId();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'currentAccountId' => $currentAccountId,
        ]);
    }

    public function actionChangeAccount($id){
        $user = User::findOne(Yii::$app->user->id);
        $user->current_account_id = $id;
        $user->save();

        Yii::$app->request->queryParams = null;
        $ref = explode('?', Yii::$app->request->referrer);

        return $this->redirect($ref[0]);
    }

    /**
     * Displays a single FinAccount model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinAccount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinAccount();
        $model->owner_id = Yii::$app->user->id;
        $model->status = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $ua = new UserAccount();
            $ua->account_id = $model->id;
            $ua->user_id = Yii::$app->user->id;
            $ua->status = 1;
            $ua->role = 'owner';
            if($ua->save()){
                Yii::$app->session->setFlash('success', Yii::t('ui', 'Your new account has been created!'));
            }

            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
                'currencies' => ArrayHelper::map(Currency::findAll(['status' => 1]), 'id', 'iso_code')
            ]);
        }
    }

    /**
     * Updates an existing FinAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'currencies' => ArrayHelper::map(Currency::findAll(['status' => 1]), 'id', 'iso_code')
            ]);
        }
    }

    /**
     * Deletes an existing FinAccount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinAccount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteUser($accountId, $userId){
        UserAccount::deleteAll([
            'user_id'=>$userId,
            'account_id'=>$accountId
        ]);

        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionAddUser($accountId, $email, $role){

        $user = User::findOne(['email'=>$email]);
        $account = FinAccount::findOne($accountId);

        if(empty($user)){
            $user = new User();
            $user->setScenario('register');
            $user->email = $email;
            $user->username = $email;
            $user->password = Password::generate(6);

            $transaction = $user->getDb()->beginTransaction();
            try {
                if (!$user->save()) {
                    $transaction->rollBack();
                }
                if ($user->module->enableConfirmation) {
                    /** @var Token $token */
                    $token = \Yii::createObject(['class' => Token::className(), 'type' => Token::TYPE_CONFIRMATION]);
                    $token->link('user', $user);
                }

                $usAccount = new UserAccount([
                    'account_id' => $accountId,
                    'user_id' => $user->id,
                    'role' => $role,
                    'status' => 1
                ]);

                if(!$usAccount->save()){
                    $transaction->rollBack();
                }

                $user->mailer->sendWelcomeMessage($user, isset($token) ? $token : null, true);
                Yii::$app->session->setFlash('success', Yii::t('ui', 'User has been invited to this account!'));

                $transaction->commit();
            } catch (Exception $e){
                $transaction->rollBack();
                \Yii::warning($e->getMessage());
                throw $e;
            }
        }
        else{
            $usAccount = UserAccount::findOne(['account_id' => $accountId,
                'user_id' => $user->id]);
            if(!empty($usAccount)){
                Yii::$app->session->setFlash('error', Yii::t('ui', 'This user already exist in this account!'));
                return $this->redirect(Yii::$app->request->referrer);
            }

            $usAccount = new UserAccount([
                'account_id' => $accountId,
                'user_id' => $user->id,
                'role' => $role,
                'status' => 1
            ]);

            $transaction = $user->getDb()->beginTransaction();
            try {
                if (!$usAccount->save()) {
                    $transaction->rollBack();
                }
                Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($email)
                    ->setSubject(Yii::$app->name . '. ' . Yii::t('ui', 'You were invited to a new account!'))
                    ->setHtmlBody(
                        "<h1>" . Yii::t('ui', 'Hello')."!</h1>
                        <p style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;\">".
                             Yii::t('ui', 'You were invited to account '). ' ' .$account->title .' '. Html::a(Html::encode(Yii::$app->urlManager->createAbsoluteUrl(['/user/login']) ) ,  Yii::$app->urlManager->createAbsoluteUrl(['/user/login'])  ) .  "!     
                        </p>"
                    )->send();
                Yii::$app->session->setFlash('success', Yii::t('ui', 'User has been invited to this account!'));

                $transaction->commit();
            }catch (Exception $e){
                $transaction->rollBack();
                \Yii::warning($e->getMessage());
                throw $e;
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}