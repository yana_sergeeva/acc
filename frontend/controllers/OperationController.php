<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Resource;
use DateTime;
use Yii;
use common\models\Operation;
use common\models\OperationSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\UserAccount;
use common\models\Card;
use common\models\FinAccount;
use common\models\Currency;
use yii\web\Response;

/**
 * OperationController implements the CRUD actions for Operation model.
 */
class OperationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

//    public function beforeAction($action)
//    {
//        if (Yii::$app->user->isGuest) {
//            return $this->redirect(['/user/login'])->send();
//        } else {
//            parent::beforeAction($action);
//        }
//    }

    /**
     * Lists all Operation models.
     * @return mixed
     */
    public function actionIndex($card = null, $to = null)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }


        $accountId = UserAccount::getFinId();
        $accData = FinAccount::findOne(['id' => $accountId]);
        $cur = Currency::findOne(['id' => $accData->default_currency_id]);

        $dataProvider = new ActiveDataProvider();
        $searchModel = new OperationSearch();

        $searchModel->account_id = $accountId;
        if(!empty($card)){
            $searchModel->card_id = $card;
        }
        if(!empty($to)){
            $searchModel->to_id = $to;
        }

        $postArray = Yii::$app->request->queryParams;


        $request = ArrayHelper::merge($postArray, ['OperationSearch' => ['account_id' => $accountId]]);


        if (!isset($postArray['OperationSearch']['date_from']) || empty($postArray['OperationSearch']['date_from'])) {
            $request = ArrayHelper::merge($request, ['OperationSearch' => ['date_from' => date('Y-m-')."01"]]);
        }


        if(Yii::$app->request->get('dp-1-sort')){
            $dataProvider = $searchModel->search($request, false);
        }else{
             $dataProvider = $searchModel->search($request, true);
        }

        $amount = $this->getTotalAmount($dataProvider);

        $dataProvider->sort = ['defaultOrder' => ['created_at'=>SORT_DESC]];
        $dataProvider->pagination = [
                'pageSize' => 20,
            ];


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'accountId' => $accountId,
            'amount' => $amount,
            'currency' => $cur->symbol,
        ]);
    }

    /**
     * Displays a single Operation model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Operation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Operation();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Updates an existing Operation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $date = new DateTime();
            if(\dektrium\user\models\Profile::findOne(Yii::$app->user->id)->timezone){
                $date->setTimezone(new \DateTimeZone(\dektrium\user\models\Profile::findOne(Yii::$app->user->id)->timezone));
            }
            $model->updated_at = $date->format($model->updated_at . ' ' . 'H:i:s');

            //$model->updated_at = (date_add(DateTime::createFromFormat('Y-m-d', $model->updated_at), date_interval__from_date_string(date("H:i:s"))));
            if($model->save()){
                return $this->redirect(['index']);
            } else {
                $accountId = UserAccount::getFinId();

                $categories =  ArrayHelper::map(Category::find()->where(['account_id' => $accountId, 'status' => Category::STATUS_ACTIVE])->all(), 'id', 'title');

                return $this->render('update', [
                    'model' => $model,
                    'categories'=>$categories
                ]);
            }
        }else {
            $accountId = UserAccount::getFinId();

            $categories =  ArrayHelper::map(Category::find()->where(['account_id' => $accountId, 'status' => Category::STATUS_ACTIVE])->all(), 'id', 'title');

            return $this->render('update', [
                'model' => $model,
                'categories'=>$categories
            ]);
        }
    }

    /**
     * Deletes an existing Operation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->deleteOperation($id)){
            Yii::$app->session->setFlash('success', "Operation was successfully removed!");
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Operation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * @param $dataProvider ActiveDataProvider
     * @return int
     */
    private function getTotalAmount($dataProvider)
    {
        $amountCount = $dataProvider;
        $amountCount->pagination = [
            'pageSize' => -1,
        ];
        $amount = 0;
        if (!empty($amountCount->getModels())) {

            foreach ($amountCount->getModels() as $key => $val) {
                if ($val->operation != 'btw') {
                    $amount += $val->amount;
                }
            }
        }
        return $amount;
    }

    public function actionAddOperation(){
        $model = new Operation();
        $accountId = UserAccount::getFinId();

        $accData = FinAccount::findOne(['id' => $accountId]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post())) {
            $model->account_id = $accountId;

            $model->user_id  = Yii::$app->user->id;
            $model->currency_id = $accData->default_currency_id;
            $model->amount = abs ( floatval($model->amount));
//            if ($model->amount <= 0) {
//                $model->addError('Operation[amount]', 'Amount must be positive.');
//            } else
            if ($model->from_id == 0 || $model->to_id == 0) {
                $model->addError('Operation[from_id]', 'Some fields are not chosen.');
                return 'Some fields are not chosen';
            } else {
//                var_dump($model->getErrors());// die();
                //            var_dump($model->validate());
                if ($model->operation == 'out')
                {
                    $model->amount = $model->amount * (-1);
                    $card = Card::find()->where(['id' => $model->from_id])->one();
                    if ($card == NULL) {
                        $model->addError('Operation[from_id]', 'Card must be chosen.');
                        return 'Card must be chosen.';
//                    } elseif ($model->amount > $card->total) {
//                        $model->addError('Operation[amount]', 'Amount cannot be more than '.number_format($card->total, 2, '.', ' '));
                    }
                }
                if ($model->operation == 'in')
                {
                    $card = Card::find()->where(['id' => $model->to_id])->one();
                    if ($card == NULL) {
                        $model->addError('Operation[to_id]', 'Card must be chosen.');
                        return 'Card must be chosen.';
                    }
                }
                if ($model->operation == 'btw')
                {
                    $card0 = Card::find()->where(['id' => $model->from_id])->one();
                    $card = Card::find()->where(['id' => $model->to_id])->one();
                    if ($card0 == NULL) {
                        $model->addError('Operation[from_id]', 'Card must be chosen.');
                        return 'Card must be chosen.';
                    }
                    if ($card == NULL) {
                        $model->addError('Operation[to_id]', 'Card must be chosen.');
                        return 'Card must be chosen.';
                    }
                    if($card == $card0){
                        $model->addError('Operation[to_id]', 'Cards must be different');
                        return 'Cards must be different';
                    }
                    if ($card0 != NULL && $card != NULL && $card != $card0) {
                        if ($card->currency_id != $card0->currency_id) {
                            $model->addError('Operation[to_id]', 'Impossible to move money between cards with different currency!');
                            return 'Impossible to move money between cards with different currency!';
                        }
//                        if ($model->amount > $card0->total) {
//                            $model->addError('Operation[amount]', 'Amount cannot be more than '.number_format($card0->total, 2, '.', ' '));
//                        }
                        $card0->total = $card0->total - $model->amount;
                        $card0->save();
                    }
                }

            }
            $date = new DateTime();

            if(\dektrium\user\models\Profile::findOne(Yii::$app->user->id)->timezone){
                $date->setTimezone(new \DateTimeZone(\dektrium\user\models\Profile::findOne(Yii::$app->user->id)->timezone));
            }


            $model->updated_at = $date->format('Y-m-d H:i:s');
            //return $model->updated_at;
            if (!$model->hasErrors())
            {
                $card->total = $card->total + $model->amount;
                if($card->type == 1 && $card->total <= 0){
                    Yii::$app->session->setFlash('error', " Not enough money on the card");
                    return 'Error! Not enough money on the card';
                }
                $model->save();
                $card->save();
                $model = new Operation();
                //Yii::$app->session->setFlash('success', "Operation was saved successful!");
                return 'Success';
//                unset($_POST['Operation']);
//                $this->refresh();
            } else {
                //Yii::$app->session->setFlash('error', \yii\helpers\Html::errorSummary($model));
                return  \yii\helpers\Html::errorSummary($model);
            }
        }

        return true;
    }

    public function actionDeleteMultiple()
    {
        if (isset($_REQUEST['selection'])) {
            foreach (Yii::$app->request->post('selection') as $select) {
                $this->deleteOperation($select);
            }
            Yii::$app->session->setFlash('success', "Operations were successfully removed!");
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function deleteOperation($id){

        $model = $this->findModel($id);

        switch ($model->operation) {
            case "in" :
                $card = Card::findOne(['id' => $model->to_id]);
                if ($card != NULL) {
                    $card->total = $card->total - abs($model->amount);
                    if ($card->total < 0 && $card->is_deleted == false) {
                        Yii::$app->session->setFlash('error', "There is no money on card to remove it!");
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                    $card->save();
                } else {
                    Yii::$app->session->setFlash('error', "Wrong card ID!");
                    return  $this->redirect(Yii::$app->request->referrer);
                }
                break;

            case "out" :
                $card = Card::findOne(['id' => $model->from_id]);
                if ($card != NULL) {
                    $card->total += abs($model->amount);
                    $card->save();
                } else {
                    Yii::$app->session->setFlash('error', "Wrong card ID!");
                    return $this->redirect(Yii::$app->request->referrer);
                }
                break;

            case "btw":
                $card1 = Card::findOne(['id' => $model->from_id]);
                $card2 = Card::findOne(['id' => $model->to_id]);
                if ($card1 != NULL && $card2 != NUll) {
                    $card1->total += abs($model->amount);

                    $card2->total = $card2->total - abs($model->amount);
                    if ($card2->total < 0 && $card2->is_deleted == false) {
                        Yii::$app->session->setFlash('error', "There is no money on card to remove it!");
                        return $this->redirect(Yii::$app->request->referrer);
                    }

                    $card1->save();
                    $card2->save();
                } else {
                    Yii::$app->session->setFlash('error', "Wrong card ID!");
                    return $this->redirect(Yii::$app->request->referrer);
                }

                break;

            default :
                Yii::$app->session->setFlash('error', "Wrong operation!");
                return $this->redirect(Yii::$app->request->referrer);
        }

        return $model->delete();
    }
}
