<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.11.17
 * Time: 16:25
 */

namespace frontend\controllers;
use yii\web\Controller;


class LandingController extends Controller
{
    public function actionIndex()
    {
        $this->layout = 'layout';
        return $this->render('index');
    }
}