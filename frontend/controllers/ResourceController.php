<?php

namespace frontend\controllers;

use Yii;
use common\models\Resource;
use common\models\ResourceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UserAccount;
use yii\helpers\ArrayHelper;

/**
 * ResourceController implements the CRUD actions for Resource model.
 */
class ResourceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

//    public function beforeAction($action)
//    {
//        if (Yii::$app->user->isGuest) {
//            return $this->redirect(['/user/login'])->send();
//        } else {
//            parent::beforeAction($action);
//        }
//    }

    /**
     * Lists all Resource models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $searchModel = new ResourceSearch();
        $request = ArrayHelper::merge(Yii::$app->request->queryParams, ['ResourceSearch' => ['account_id' => UserAccount::getFinId(),'is_deleted'=> false]]);
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Resource model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Resource model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $model = new Resource();
        $model->account_id = UserAccount::getFinId();
        $model->status = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Resource model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Resource model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_deleted = true;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Resource model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resource the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Resource::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionArchive($id){
        $resource = Resource::findOne($id);
        $resource->status = $resource->status==Resource::STATUS_ACTIVE
            ? Resource::STATUS_DISACTIVE
            : Resource::STATUS_ACTIVE;
        $resource->save();

        return $this->redirect(['index']);
    }

    public function actionDeleteMultiple(){
        if(isset($_REQUEST['selection'])){
            foreach (Yii::$app->request->post('selection') as $select){
                $card = Resource::findOne($select);
                $card->is_deleted = true;
                $card->save();
            }
            Yii::$app->session->setFlash('success', "Categories were successfully removed!");
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
