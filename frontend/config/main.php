<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name'     => 'MultiWallet',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => ['log', 'translatemanager', 'languagepicker'],
    'language'            => 'en',

    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@frontend/views'
                ],
            ],
        ],

        'translatemanager' => [
            'class' => 'lajax\translatemanager\Component'
        ],
        'languagepicker' => [
            'class' => 'lajax\languagepicker\Component',
            'languages' =>['en'=>'English', 'ru'=>'Русский' ],
//                function () {                        // List of available languages (icons and text)
//                return \lajax\translatemanager\models\Language::getLanguageNames(true);
//            },
            'cookieName' => '_language',                         // Name of the cookie.
            'cookieDomain' => $params['domain'],                    // Domain of the cookie.
            'expireDays' => 64,                                 // The expiration time of the cookie is 64 days.
            'callback' => function() {
                if (!\Yii::$app->user->isGuest) {
                    $user = \Yii::$app->user->identity;
                    $user->language = \Yii::$app->language;
                    $user->save();
                }
//                var_dump($_SERVER); die();
            }
        ],

//        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//        ],
        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'class' => 'frontend\models\User',
            'identityClass' => 'common\models\User',
            'on '.\yii\web\User::EVENT_AFTER_LOGIN => ['frontend\events\UserEvents', 'handleAfterLogin'],
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                // here is the list of clients you want to use
                // you can read more in the "Available clients" section
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '1070011459760108',
                    'clientSecret' => 'dc8418296d744851e543d31db09033ad',
                ],
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => '178236982110-a777fu4mtkm0gu6og2t90vn82m6g9sp4.apps.googleusercontent.com',
                    'clientSecret' => '1Af_wzAvuE6RmzwHQnWFiwe4',
                ],
            ],
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['en', 'ru'],
//            'enableDefaultLanguageUrlCode' => true,
            'languageParam' => 'language-picker-language',
            'rules' => [
                'lang' => 'translatemanager/language/list',

                '/' => 'site/index',
                '<action:[\w\-]+>' => 'site/<action>',
                'statistic/index/<year:\d+>/<month:\d+>' => 'statistic/index',

                '<controller:[\w\-]+>/<id:\d+>' => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
            ]
        ],


    ],
    'modules' => [
        'gridview' => ['class' => 'kartik\grid\Module'],
        'user' => [
            'class' => 'dektrium\user\Module',
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
            'enableUnconfirmedLogin' => true,
            'enableGeneratingPassword' => true,
            'enableFlashMessages' => false,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['ys.tempos@gmail.com'],
            'modelMap' => [
                'User' => 'common\models\User',
                'ResendForm' => 'frontend\models\ResendForm'
            ],
//            'class' => 'dektrium\user\Module',
//            'modelMap' => [
//                'User' => 'frontend\models\User',
//            ],
//            'controllerMap' => [
//                'admin' => [
//                    'class'  => 'dektrium\user\controllers\AdminController',
//                    'layout' => '//admin-layout',
//                ],
//            ],
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'root' => '@app',
//            'root' => '@backend',               // The root directory of the project scan.
            'scanRootParentDirectory' => true, // Whether scan the defined `root` parent directory, or the folder itself.
                                               // IMPORTANT: for detailed instructions read the chapter about root configuration.
//            'layout' => '/main',         // Name of the used layout. If using own layout use 'null'.
            'allowedIPs' => ['127.0.0.1', '193.161.14.17'],  // IP addresses from which the translation interface is accessible.
            'roles' => ['@'],               // For setting access levels to the translating interface.
            'tmpDir' => '@runtime',         // Writable directory for the client-side temporary language files.
                                            // IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
            'phpTranslators' => ['::t'],    // list of the php function for translating messages.
            'jsTranslators' => ['lajax.t'], // list of the js function for translating messages.
            'patterns' => ['*.js', '*.php'],// list of file extensions that contain language elements.
            'ignoredCategories' => ['yii'], // these categories won't be included in the language database.
            'ignoredItems' => ['config'],   // these files will not be processed.
            'scanTimeLimit' => null,        // increase to prevent "Maximum execution time" errors, if null the default max_execution_time will be used
            'searchEmptyCommand' => '!',    // the search string to enter in the 'Translation' search field to find not yet translated items, set to null to disable this feature
            'defaultExportStatus' => 1,     // the default selection of languages to export, set to 0 to select all languages by default
            'defaultExportFormat' => 'json',// the default format for export, can be 'json' or 'xml'
            'tables' => [                   // Properties of individual tables
                [
                    'connection' => 'db',   // connection identifier
                    'table' => '{{%language}}',         // table name
                    'columns' => ['name', 'name_ascii'],// names of multilingual fields
//                    'category' => 'database-table-name',// the category is the database table name
                ]
            ]
        ],
    ],
    'params' => $params,
];
