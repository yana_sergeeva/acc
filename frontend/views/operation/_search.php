<?php

use common\models\Card;
use common\models\Resource;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Category;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\OperationSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="operation-search">

    <?php $form = ActiveForm::begin([
        'id' => 'search-history',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'account_id') ?>

    <?php // echo $form->field($model, 'to_type') ?>

    <div class="row">
        <div class="col-md-3">
            <?php echo $form->field($model, 'card_id')->dropDownList(
                ArrayHelper::merge(['' => Yii::t('ui', "All")],
                    ArrayHelper::map(
                        Card::find()->where(['account_id' => $accountId])->andWhere(['is_deleted' => false])->all(), 'id', 'title')),['class' => 'input-field'])
                ->label(Yii::t('ui', 'Card')) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'to_id')->dropDownList(
            ArrayHelper::merge(['' => Yii::t('ui', "All")],
            ArrayHelper::map(
                    Category::find()->where(['account_id' => $accountId])->andWhere(['is_deleted' => false])->all(), 'id', 'title')),['class' => 'input-field'])
            ->label(Yii::t('ui', 'Category')) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'from_id')->dropDownList(
                ArrayHelper::merge(['' => Yii::t('ui', "All")],
                    ArrayHelper::map(
                        Resource::find()->where(['account_id' => $accountId])->andWhere(['is_deleted' => false])->all(), 'id', 'title')),['class' => 'input-field'])
                ->label(Yii::t('ui', 'Resource')) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'operation')->dropDownList([
                '' => Yii::t('ui', "All"),
                'out' => Yii::t('ui', "Expenses"),
                'in' => Yii::t('ui', "Income"),
                'btw' => Yii::t('ui', "Move"),
            ],['class' => 'input-field']); ?>
        </div>
    </div>

    <?php // echo $form->field($model, 'amount') ?>
    <div class="row">
        <div class="col-md-3">
    <?php echo $form->field($model, 'date_from')->widget(DatePicker::className(), [
            'language'   => (Yii::$app->language == 'ru') ? 'ru-RU' : 'en-US',
            'dateFormat' => 'yyyy-MM-dd',
        ])->textInput(['class'=>'form-control', 'readonly'=>true])->label(Yii::t('ui', "Date From")) ?>
        </div>
        <div class="col-md-3">
    <?php echo $form->field($model, 'date_to')->widget(DatePicker::className(), [
            'language'   => (Yii::$app->language == 'ru') ? 'ru-RU' : 'en-US',
            'dateFormat' => 'yyyy-MM-dd',
        ])->textInput(['class'=>'form-control', 'readonly'=>true])->label(Yii::t('ui', "Date To")) ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->field($model, 'comments')->textInput(['class' => 'input-field']) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['style' => 'margin-bottom: 10px' ,'class' => 'btn waves-effect waves-light cyan']) ?>
        <?= Html::Button(Yii::t('app', 'Reset'), ['id' => 'reset-form','style' => 'margin-bottom: 10px; color:black; margin-left: 1%','class' => 'btn waves-effect grey lighten-3']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
