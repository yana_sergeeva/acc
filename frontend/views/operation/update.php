<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Operation */
/* @var $categories array */

$this->title = Yii::t('app', 'Update Operation #') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="operation-update">

    <?= $this->render('_form', [
        'model' => $model,
        'categories'=>$categories,
    ]) ?>

</div>
