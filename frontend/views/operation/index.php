<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Operation;
use common\models\Card;
use common\models\Resource;
use common\models\Category;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use common\models\User;



/* @var $this yii\web\View */
/* @var $searchModel common\models\OperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $fromItems [] */

$this->registerCssFile('/css/operation.css');
$this->title = Yii::t('app', 'Operations');
$this->params['breadcrumbs'][] = $this->title;


?>

<?php $columns = [
    [],
    [
        'attribute' => 'updated_at',
        'label' => Yii::t('ui', 'Date'),
        'contentOptions' => ['data-title' => Yii::t('ui', 'Date')],
        'format' => 'html',
        'value' => function ($data) {
            return date("d/m/Y", strtotime($data->updated_at))  . ' ' . date("H:i:s", strtotime($data->updated_at));
        },

    ],

    [
        'attribute' => 'from',
        'label' => Yii::t('ui', 'From'),
        'contentOptions' => ['data-title' => Yii::t('ui', 'From')],
        'format' => 'html',
        'value' => function ($data) {
            switch ($data->from_type) {
                case 'card' :
                    if ( Card::find()->where(['id' => $data->from_id])->exists()  ) {
                        $from = Card::find()->where(['id' => $data->from_id])->one();
                        $title = $from->title;
                    } else {
                        $title = '';
                    }
                    break;

                case 'category' :
                    if (Category::find()->where(['id' => $data->from_id])->exists()) {
                        $from = Category::find()->where(['id' => $data->from_id])->one();
                        $title = $from->title;
                    } else {
                        $title = '';
                    }
                    break;

                case 'resource' :
                    if (Resource::find()->where(['id' => $data->from_id])->exists()) {
                        $from = Resource::find()->where(['id' => $data->from_id])->one();
                        $title = $from->title;
                    } else {
                        $title = '';
                    }
                    break;

            }
            return $title;
        },
    ],
    [
        'attribute' => 'to',
        'label' => Yii::t('ui', 'To'),
        'contentOptions' => ['data-title' => Yii::t('ui', 'To')],
        'format' => 'html',
        'value' => function ($data) {
            switch ($data->to_type) {
                case 'card' :
                    if (Card::find()->where(['id' => $data->to_id])->exists() ) {
                        $from = Card::find()->where(['id' => $data->to_id])->one();
                        $title = $from->title;
                    } else {
                        $title = '';
                    }
                    break;

                case 'category' :
                    if (Category::find()->where(['id' => $data->to_id])->exists()) {
                        $from = Category::find()->where(['id' => $data->to_id])->one();
                        $title = $from->title;
                    } else {
                        $title = '';
                    }
                    break;

                case 'resource' :
                    if (Resource::find()->where(['id' => $data->to_id])->exists()) {
                        $from = Resource::find()->where(['id' => $data->to_id])->one();
                        $title = $from->title;
                    } else {
                        $title = '';
                    }
                    break;
            }
            return $title;
        },
    ],
    [
        'attribute' => 'amount',
        'label' => Yii::t('app', 'Amount') . ", " . $currency,
        'contentOptions' => ['data-title' => Yii::t('app', 'Amount') . ", " . $currency],
        'format' => 'html',
        'value' => function ($data) {
            return number_format($data->amount, 2, '.', ' ');
        },
        //                'footer' => $amount,
    ],

    [
        'attribute' => 'comments',
        'contentOptions' => ['data-title' => Yii::t('ui', 'Comments')],
        'format' => 'html',
        'value' => function ($data) {
            return empty($data->comments) ? "-" : nl2br($data->comments);
        }
    ],

    [
        'attribute' => 'user_id',
        'contentOptions' => ['data-title' => Yii::t('ui', 'Creator')],
        'format' => 'raw',
        'value' => function ($data) {
            $user = User::findOne($data->user_id);
            return $user->profile->name ? $user->profile->name : $user->username;
        }
    ]];

$columns1 = $columns;
array_shift($columns);
?>



<div class="operation-index">
    <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
        <li style="margin: 0">
            <div class="collapsible-header">
                <span class="box-title"><i class="fa fa-filter"></i> <?= Yii::t('ui', 'Filter') ?></span>
            </div>
            <div class="collapsible-body">
                <?php echo $this->render('_search', ['model' => $searchModel, 'accountId' => $accountId]); ?>
            </div>
        </li>
    </ul>
</div>

<div class="operation-index">
    <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
        <li style="margin: 0" class="active">
            <div class="collapsible-header active">
                <span class="box-title"><i class="fa fa-history"></i> <?= Yii::t('ui', 'Operation history') ?></span>
            </div>
            <div style="overflow: auto" class="collapsible-body">
                <?php Pjax::begin(); ?>

                <h4 style="font-size: 24px;margin-top: 0px" class="text-center"><?= Yii::t('app', 'Total:') ?>
                    <b><?= number_format($amount, 2, '.', ' ') . $currency; ?></b></h4>
                <div id="page-wrapper">
                    <div id="mult-del" style="margin-bottom: 20px; float: right">
                        <button style="background-color: #ff4081!important" id="submit-delete" class="btn btn-info">Delete multiple</button>
                    </div>
                    <?= ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'dropdownOptions' => [
                            'label' => 'Export All',
                            'class' => 'btn btn-default'
                        ]
                    ]) ?>
                    <?php echo "<hr>\n" ?>
                    <?=Html::beginForm(['operation/delete-multiple'],'post', ['id' => 'form-statistic']);?>

                    <?= GridView::widget(
                        [
                            'dataProvider' => $dataProvider,
                            'options' => ['class' => 'mobile-friendly-table mas-delete-table'],
                            //'filterModel' => $searchModel,
                            'pjax'=>true,
                            'rowOptions' => function ($data) {
                                switch ($data->operation) {
                                    case Operation::OPERATOR_IN :
                                        return ['class' => 'text-success'];
                                        break;

                                    case Operation::OPERATOR_OUT :
                                        return ['class' => 'text-danger'];
                                        break;

                                    case Operation::OPERATOR_BETWEEN :
                                        return ['class' => 'text-info'];
                                        break;

                                    default:
                                        return ['class' => 'text-danger'];
                                }
                            },
                            'columns' => [
                                    0 => [
                                        'class' => 'yii\grid\SerialColumn',
                                        'contentOptions' => ['data-title' => '#'],
                                    ]] + $columns1 + [7 =>[
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update} {delete}'
                                ]] +
                                [8 => ['class' => 'yii\grid\CheckboxColumn',
                                    'header' => Html::checkBox('selection_all', false, [
                                        'class' => 'select-on-check-all',
                                        'label' => 'Check All',
                                    ]),
                                    'checkboxOptions' => function($model) {
                                        return ['value' => $model->id, 'class' => 'filled-in'];
                                    },
                                ]],
                        ]);
                    ?>
                </div>
                <?php Pjax::end(); ?>
                <?= Html::endForm();?>
            </div>

        </li>
    </ul>
</div>
