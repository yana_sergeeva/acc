<?php


use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Operation */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories array */
?>

<div class="operation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($model->from_type=='card' && $model->operation == 'out' && $model->to_type == 'category'): ?>
        <?= $form->field($model, 'to_id')->dropDownList($categories,['class' => 'input-field'])->label(Yii::t('ui', 'To Category')) ?>
    <?php endif;?>

    <?=$form->field($model, 'updated_at')->widget(DatePicker::className(), [
    'language'   => (Yii::$app->language == 'ru') ? 'ru-RU' : 'en-US',
    'dateFormat' => 'yyyy-MM-dd',
        'options'=>[
            'class'=>'form-control',
            'readOnly'=>true
        ]
    ])?>

    <?= $form->field($model, 'comments')->textarea(['id'=> 'operation-comment','maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
