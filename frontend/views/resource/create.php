<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Resource */

$this->title = Yii::t('app', 'Create Resource');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Resources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-create">

<!--    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
         /.col-lg-12
    </div>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
