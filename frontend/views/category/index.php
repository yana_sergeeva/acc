<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/input.css');

?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body mobile-friendly-table">
                <div id="mult-del" style="margin-bottom: 20px; float: right">
                    <button style="background-color: #ff4081!important" id="submit-delete" class="btn btn-info">Delete multiple</button>
                </div>
                <p>
                    <?= Html::a("<i class='fa fa-plus'></i> ".Yii::t('app', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php Pjax::begin(); ?>
                <?=Html::beginForm(['category/delete-multiple'],'post', ['id' => 'form-statistic']);?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'contentOptions' => ['data-title' => '#'],
                        ],
                        [
                            'attribute' => 'title',
                            'contentOptions' => ['data-title' => 'Category'],
                        ],
                        [
                            'attribute' => 'status',
                            'value'=> function($data){
                                return $data->status?Yii::t('app', 'Active'):Yii::t('app', 'Not active');
                            },
                            'contentOptions' => ['data-title' => 'Status'],
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;{archive}',
                            'buttons'=> [
                                'archive'=>function($url, $model, $key){
                                    return $model->status
                                        ? Html::a('<span class="glyphicon glyphicon-floppy-remove"></span>',
                                            Url::to(['/category/archive', 'id'=>$model->id]),
                                            ['title' => Yii::t('app', 'Disactivate')])
                                        : Html::a('<span class="glyphicon glyphicon-floppy-saved"></span>',
                                            Url::to(['/category/archive', 'id'=>$model->id]),
                                            ['title' => Yii::t('app', 'Disactivate')]);
                                }
                            ]
                        ],
                        ['class' => 'yii\grid\CheckboxColumn',
                            'header' => Html::checkBox('selection_all', false, [
                                'class' => 'select-on-check-all',
                                'label' => 'Check All',
                            ]),
                            'checkboxOptions' => function($model) {
                                return ['value' => $model->id, 'class' => 'filled-in'];
                            },
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
                <?= Html::endForm();?>
            </div>
        </div>
    </div>
</div>
