<?php
/* @var $this \yii\web\View */

use kartik\tabs\TabsX;
use yii\helpers\Url;

/* @var $content string */
$this->registerCssFile('/css/startuply.css');
$this->registerCssFile('/css/Startup/style.css');


$this->registerJsFile('/js/startuply/startuply.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJSFile('/js/landing-index.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>



<div id="mask">

    <div class="preloader">
        <div class="spin base_clr_brd">
            <div class="clip left">
                <div class="circle"></div>
            </div>

        </div>
    </div>

</div>

<header>
    <nav class="navigation navigation-header" role="navigation">
            <div class="container">
                <div class="navigation-brand">
                    <div class="brand-logo vertically-centered">

                        <a style="display: inline-flex" href="/dashboard" class="logo">
                            <img style="height: 50px;width: 90px " src="/images/landing/LOGO.png"
                                 height="27" width="111" alt="MultiWallet"/>
                            <img
                                    style="height: 50px;width: 90px;" src="/images/landing/LOGO.png"
                                    height="29" width="111" class="sticky-logo" alt="MultiWallet"/>
                            <span style="font-size: 35px; text-decoration: none; color: black">MultiWallet</span></a>
                    </div>
                </div>
                <button id="menu-button-invisible" class="navigation-toggle visible-xs" type="button" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="make-it-invisible" class="navigation-navbar navbar-collapse collapsed">
                    <div class="menu-wrapper">
                        <!-- Left menu -->
                        <div class="menu-loginregister-container" style="float: right">
                            <ul id="menu-loginregister" class="navigation-bar navigation-bar-right">
                                <li id="menu-item-4796"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4796"><a
                                            title="LOGIN"
                                            href="<?= Url::to(['/user/login']) ?>">LOGIN</a></li>
                                <li id="menu-item-4797"
                                    class="featured menu-item menu-item-type-post_type menu-item-object-page menu-item-4797">
                                    <a title="REGISTER"
                                       href="<?= Url::to(['/user/register']) ?>">REGISTER</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

<div id="main-content" class="content-area" >

    <div class="container-fluid">

        <div id="home" class="parallax  vc_row wpb_row vc_inner vc_row-fluid vc_custom_1458900249119 light row"
             style="height:50vw;padding-top: 325.5px;padding-bottom: 325.5px; background-color: #74c8d9; background-image: url('/images/206e96c9e34b100723e1eb570286ab0f00f621f5_1200.jpg');background-repeat:no-repeat;"
             data-token="lj0fW">
            <div class="vc_col-sm-12 wpb_column column_container  aligncenter">
                <div class="wpb_wrapper">
                    <div id="vsc_row_pittshaqwp" class="  vc_row wpb_row vc_inner vc_row-fluid dark container" style=""
                         data-token="soYUg">
                        <div class='wpb_animate_when_almost_visible gambit-css-animation fade-in'
                             style='opacity: 1;-webkit-animation-duration: 0.7s;-moz-animation-duration: 0.7s;-ms-animation-duration: 0.7s;-o-animation-duration: 0.7s;animation-duration: 0.7s'>
                            <div class="vc_col-sm-12 wpb_column column_container  ">
                                <div style="opacity: 1" class="wpb_wrapper">
                                    <div class="section-title "><h1 style="font-size: 24px; color: black">НАЧНИ ВЕСТИ
                                            <strong>ДОМАШНЮЮ БУХГАЛТЕРИЮ </strong>
                                            СЕЙЧАС!</h1>
                                        <p style="color: black">Программа разработана для учета расходов и доходов бюджетов семьи и личных финансов.
                                        </p>
                                    </div>
                                    <a style="margin-top: 20px" class=" btn btn-solid"
                                       href="#anchor">
                                        Как работает</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="vsc_row_wospfgmsuq"
         class="  vc_row wpb_row vc_inner vc_row-fluid vc_custom_1423845168912 dark container" style=""
         data-token="Wev0w">
        <div class="vc_col-sm-12 wpb_column column_container  ">
            <div class="wpb_wrapper">
                <div class='wpb_animate_when_almost_visible gambit-css-animation slide-to-right'
                     style='opacity: 1;-webkit-animation-duration: 0.5s;-moz-animation-duration: 0.5s;-ms-animation-duration: 0.5s;-o-animation-duration: 0.5s;animation-duration: 0.5s'>
                    <div id="vsc_row_qedjpougpl" class="  vc_row wpb_row vc_inner vc_row-fluid dark container" style=""
                         data-token="AC3Y2">
                        <div class="vc_col-sm-3 wpb_column column_container  ">
                            <div class="wpb_wrapper">
                                <article style=""
                                         class="vsc-text-icon clearfix  icon-top icon-single icon-round icon-border-solid">
                                    <div class="vsc-service-icon"><img style="width: auto" class="icon" src="/images/landing/abacus1.png" style=""></img></div>
                                    <div class="vsc-service-content"><h5>ПРОСТО и ПОНЯТНО</h5>
                                        <p>Простой, интуитивно понятный каждому интерфейс. Вы создаете категории расходов и доходов. Вносите статистику по дням. Анализируете бюджет за любой период.
                                        </p></div>
                                </article>
                            </div>
                        </div>

                        <div class="vc_col-sm-3 wpb_column column_container  ">
                            <div class="wpb_wrapper">
                                <article style=""
                                         class="vsc-text-icon clearfix  icon-top icon-single icon-round icon-border-solid">
                                    <div class="vsc-service-icon"><img style="width: auto" class="icon" src="/images/landing/family1.png" style=""></img></div>
                                    <div class="vsc-service-content"><h5>ДЛЯ ВСЕЙ СЕМЬИ</h5>
                                        <p>Один кабинет для всей семьи!  Если у Вас общий бюджет - данная функция незаменима. Вы увидите движения средств, доходы и расходы всех членов семьи.
                                        </p></div>
                                </article>
                            </div>
                        </div>

                        <div class="vc_col-sm-3 wpb_column column_container  ">
                            <div class="wpb_wrapper">
                                <article style=""
                                         class="vsc-text-icon clearfix  icon-top icon-single icon-round icon-border-solid">
                                    <div class="vsc-service-icon"><img style="width: auto" class="icon" src="/images/landing/responsive1.png" style=""></img></div>
                                    <div class="vsc-service-content"><h5>ВСЕГДА ПОД РУКОЙ</h5>
                                        <p>Для удобства пользователей разработаны Android и iOS приложения для смартфонов!
                                            Контролируйте состояние семейного бюджета в любое время с любого места!
                                        </p></div>
                                </article>
                            </div>
                        </div>


                        <div class="vc_col-sm-3 wpb_column column_container  ">
                            <div class="wpb_wrapper">
                                <article style=""
                                         class="vsc-text-icon clearfix  icon-top icon-single icon-round icon-border-solid">
                                    <div class="vsc-service-icon"><img style="width: auto" class="icon" src="/images/landing/piggy-bank1.png" style=""></img></div>
                                    <div class="vsc-service-content"><h5>ПОНИМАЙ и КОПИ</h5>
                                        <p>С помощью данной программы Вы поймете откуда и куда уходят средства. А  “понять”  - это пол дела к “улучшить”. Откладывайте в “копилку” и мечтайте вместе с MultiWallet!</p></div>
                                </article>
                            </div>

                        </div>

                    </div>
                    <div  id="anchor"></div>
                </div>


                <div class="vc_empty_space" style="height: 35px"><span class="vc_empty_space_inner"></span></div>
            </div>
        </div>

    </div>
    </div>



    <div id="features" class="  vc_row wpb_row vc_inner vc_row-fluid dark row" style="margin-top: 60px;margin-right: 0; margin-left: 0" data-token="hNrn4">
        <div class="container">
            <div style="margin-bottom: 30px" class="row">
               <div class="col-lg-6 col-sm-6">
                    <div style="text-align: center" class="row">
                        <span >
                            Начать пользоваться сервисом очень просто!
                        </span>
                    </div>
                    <div style="margin-top: 20px; text-align: center" class="row">
                        <span style="text-align: center">
                            С первых дней ведения домашнего учета Вы поймете насколько это упрощает жизнь и направляет ее в правильно русло!
                        </span>
                    </div>
                    <div style="margin-top: 50px; margin-bottom: 20px; text-align: center" class="row">
                        <i class="phone-sign"></i>
                    </div>
               </div>

               <div class="col-lg-6 col-sm-6">
                   <?php
                   echo TabsX::widget([
                       'position' => TabsX::POS_ABOVE,
                       'align' => TabsX::ALIGN_CENTER,
                       'items' => [
                           [
                               'label' => '1',
                               'content' => '<div style="margin-top: 20px" class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div style="margin-bottom: 40px" class="row">
                                            <span>Создание категорий </span>
                                        </div>
                                        <div style="margin-bottom: 10px" class="row">
                                            <div class="tabs-number"><span style="text-align: center; position: absolute; top: -5px;  left: 10px;">1</span></div>
                                            <span>Создаете категории доходов (зарплата, бонусы, подработка, т.п.) </span>
                                        </div>
                                        <div style="margin-bottom: 10px" class="row">
                                            <div class="tabs-number"><span style="text-align: center; position: absolute; top: -5px;  left: 10px;">2</span></div>
                                            <span>Создаете категории возможных расходов (транспорт, еда, одежда, т.п.)  </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <i class="tabs-image"></i>
                                    </div>
                                </div>',
                               'headerOptions' => ['style'=>'font-weight:bold'],
                               'options' => ['id' => 'ID_0'],
                               'active' => true
                           ],
                           [
                               'label' => '2',
                               'content' => '<div style="margin-top: 20px" class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div style="margin-bottom: 40px" class="row">
                                            <span>Создание категорий </span>
                                        </div>
                                        <div style="margin-bottom: 10px" class="row">
                                            <div class="tabs-number"><span style="text-align: center; position: absolute; top: -5px;  left: 10px;">1</span></div>
                                            <span>Создаете категории доходов (зарплата, бонусы, подработка, т.п.) </span>
                                        </div>
                                        <div style="margin-bottom: 10px" class="row">
                                            <div class="tabs-number"><span style="text-align: center; position: absolute; top: -5px;  left: 10px;">2</span></div>
                                            <span>Создаете категории возможных расходов (транспорт, еда, одежда, т.п.)  </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <i class="tabs-image"></i>
                                    </div>
                                </div>',
                               'headerOptions' => ['style'=>'font-weight:bold'],
                               'options' => ['id' => 'ID_1'],
                           ],
                           [
                               'label' => '3',
                               'content' => '<div style="margin-top: 20px" class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div style="margin-bottom: 40px" class="row">
                                            <span>Создание категорий </span>
                                        </div>
                                        <div style="margin-bottom: 10px" class="row">
                                            <div class="tabs-number"><span style="text-align: center; position: absolute; top: -5px;  left: 10px;">1</span></div>
                                            <span>Создаете категории доходов (зарплата, бонусы, подработка, т.п.) </span>
                                        </div>
                                        <div style="margin-bottom: 10px" class="row">
                                            <div class="tabs-number"><span style="text-align: center; position: absolute; top: -5px;  left: 10px;">2</span></div>
                                            <span>Создаете категории возможных расходов (транспорт, еда, одежда, т.п.)  </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <i class="tabs-image"></i>
                                    </div>
                                </div>',
                               'headerOptions' => ['style'=>'font-weight:bold'],
                               'options' => ['id' => 'ID_2'],
                           ],
                       ],
                   ]);
                   ?>
               </div>

            </div>
        </div>

    </div>

    <div class="row">
        <div id="app-block" style="margin-left: 20%; margin-right:20%; width: 60%">
            <div id="cta_footer_0" class="parallax  vc_row wpb_row vc_inner vc_row-fluid vc_custom_1489794819977 light row"
                 style="background-image: url(http://demo.startuplywp.com/wp-content/uploads/2017/02/startuply_bg.jpg);background-repeat:no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;background-position: center center;"
                 data-token="hYH91">
                <div class="container">
                    <div class="vc_col-sm-12 wpb_column column_container  ">
                        <div class="wpb_wrapper">

                            <div style="text-align: center" class="cta-block ">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                        <span>Скачать приложение для смартфона</span>
                                    </div>
                                </div>
                            </div>
                            <div style="text-align: center" class="row">
                                <a href="/android/acc.temposys.name.apk" style="min-width: 200px; padding: 14px 25px" class="btn btn-default button-download">
                                    <i class="android"></i>Android</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            </div>
        </div>
    </div>


    <div id="facts" class="  vc_row wpb_row vc_inner vc_row-fluid dark container" style="" data-token="JLLjg">
        <div style="margin-top:25px; text-align: center" class="row">
            <span >Приложение было скачано более 1500 человек!</span>
        </div>
        <div style="text-align: center; margin-bottom: 30px;" class="row">
            <span>Спасибо Вам за поддержку сервиса. Благодаря Вам мы развиваемся!</span>
        </div>

        <div class="row">
            <div class="col-lg-5 col-sm-12 reviews-box ">
                <img src="/images/favicon/favicon-32x32.png"  style="width: 100px; height: 110px;top: 0; left: -10px; object-fit: contain">
                <div style="display: inline-block; top: 10px; position: absolute; margin-left: 10px;">
                    <h style="top: 10px">Игорь</h>
                    <i style="float: right; margin-left: 10px" class="facebook-sign"></i>
                </div>
                <div style="display: inline-block; top: 50px; position: absolute; margin-left: 10px;">
                    <span class="text-review">Пользуюсь сервисом MultiWallet больше года, веду в нем домашнюю бухгалтерию
                        с женой и бухгалтерию по бизнесу с партнером. Удобно переключаться между аккаунтами, все важные
                        показатели всегда на виду. Есть мобильное приложение, что упрощает введение расходов, когда ты не дома.
                        Спасибо. Пользуюсь и доволен! </span>
                </div>
            </div>
            <div class="col-lg-offset-2 col-lg-5 col-sm-12 reviews-box ">
                <img src="/images/favicon/favicon-32x32.png"  style="width: 100px; height: 110px;top: 0; left: 0; object-fit: contain">
                <div style="display: inline-block; top: 10px; position: absolute; margin-left: 10px">
                    <h style="top: 10px">Екатерина</h>
                    <i style="float: right; margin-left: 10px" class="facebook-sign"></i>
                </div>
                <div style="display: inline-block; top: 50px; position: absolute; margin-left: 10px">
                    <span class="text-review">Работаю не так давно с MW, но уже без него обойтись не смогу) Заводила
                        чисто для домашнего использования. Ранее просто все в Excel вела и цветами выделяла… А вот с
                        сервисом все намного проще. Удобный интерфейс, ничего лишнего. Круто, что есть приложение для мобильного.
                        Всегда и везде могу внести приход/расход  и ничего не потеряется!)</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div  class="col-lg-5 col-sm-12 reviews-box ">
                <img src="/images/favicon/favicon-32x32.png"  style="width: 100px; height: 110px; top: 0; left: 0; object-fit: contain">
                <div style="display: inline-block; top: 10px; position: absolute; margin-left: 10px">
                    <h style="top: 10px">Татьяна</h>
                    <i style="float: right; margin-left: 10px" class="facebook-sign"></i>
                </div>
                <div style="display: inline-block; top: 50px; position: absolute; margin-left: 10px">
                    <span class="text-review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </span>
                </div>
            </div>
            <div class="col-lg-offset-2 col-lg-5 col-sm-12 reviews-box ">
                <img src="/images/favicon/favicon-32x32.png"  style="width: 100px; height: 110px; top: 0; left: 0; object-fit: contain">
                <div style="display: inline-block; top: 10px; position: absolute; margin-left: 10px">
                    <h style="top: 10px">Татьяна</h>
                    <i style="float: right; margin-left: 10px" class="facebook-sign"></i>
                </div>
                <div style="display: inline-block; top: 50px; position: absolute; margin-left: 10px">
                    <span class="text-review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </span>
                </div>
            </div>
        </div>
    </div>


    <div id="cta_footer" class="parallax  vc_row wpb_row vc_inner vc_row-fluid vc_custom_1489794819977 light row"
             style="padding-right: 15px;padding-left: 15px;background-image: url(http://demo.startuplywp.com/wp-content/uploads/2017/02/startuply_bg.jpg);background-repeat:no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;background-position: center center;"
             data-token="hYH91">
        <div style="max-width: inherit; width: 100%;margin: 0" class="container ">
            <div class="row">

                <div class="col-sm-2 col-lg-2 frame">
                    <span class="helper"></span>
                    <img class="image-middle" src="/images/favicon/favicon-32x32.png"  style="width: 100px; height: 250px;object-fit: contain">
                </div>

                <div class="col-sm-10 col-lg-10 make-margin">
                    <div style="margin-top: 20px; margin-bottom: 40px" class="row">
                        <span>
                          О создании сервиса
                        </span>
                    </div>

                    <div class="row"?>
                        <span style="display: inline-block">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </span>
                    </div>

                    <div style="margin-top: 30px" class="row">
                        <span style="display: inline-block">
                            Сергеева Я.В.
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>


<footer id="footer" class="footer enabled ">
    <div class="container">


        <div class="row">

            <div class="col-sm-3">
                <aside id="about_sply_widget-77" class="widget widget_about">
                    <div class="widgetBody clearfix">
                        <div class="logo-wrapper">
                            <a href="/dashboard" class="logo">
                                <span style="font-size: 20px; color: white">Multiwallet</span>
                        </div>
                        <p>
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco. Qui officia deserunt mollit anim
                            id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco. <br>
                            <strong>John Doeson, Founder.</strong>
                        </p>

                    </div>
                </aside>
            </div>
            <div class="col-sm-3">
                <aside id="socials_sply_widget-77" class="widget widget_socials">
                    <div class="widgetBody clearfix"><h2 class="widgettitle">Social Networks</h2>


                        <ul class="list-inline socials">
                            <li><a href="#" target="_blank"><span
                                            class="icon icon-socialmedia-08"></span></a></li>
                            <li><a href="#" target="_blank"><span
                                            class="icon icon-socialmedia-16"></span></a></li>
                            <li><a href="#" target="_blank"><span
                                            class="icon icon-socialmedia-09"></span></a></li>
                        </ul>


                    </div>
                </aside>
            </div>
            <div class="col-sm-3">
                <aside id="contacts_sply_widget-77" class="widget widget_contacts">
                    <div class="widgetBody clearfix"><h2 class="widgettitle">Our Contacts</h2>
                        <ul class="list-unstyled">
                            <li>
                                <span class="icon icon-chat-messages-14"></span>
                                <a href="mailto:office@vivaco.com">office@vivaco.com</a>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="col-sm-3"></div>
        </div>

    </div>
</footer>


<div class="back-to-top"><a href="#"><i class="fa fa-angle-up fa-3x"></i></a></div>

<div class="modal-window vivaco-3627 " data-modal="vivaco-3627" data-overlay="1"
     style="background-color:rgba(0,0,0,0.85);">
    <div class="modal-box custom  animated slideInDown hide_bg" data-animation="slideInDown" data-duration="0.5"><span
                class="close-btn icon icon-office-52"></span>
        <p>
        <div id="vsc_row_yrbqmkdxrl" class="  vc_row wpb_row vc_inner vc_row-fluid dark container" style=""
             data-token="s2pqb">
            <div class="vc_col-sm-12 wpb_column column_container  ">
                <div class="wpb_wrapper">


                </div>
            </div>
        </div>
        </p>
    </div>
</div>


</body>
</html>
