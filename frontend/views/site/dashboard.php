<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
//use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Card;
use common\models\Operation;
use common\models\Resource;
use common\models\Category;
use yii\widgets\Pjax;
use kartik\grid\GridView;

$this->title = Yii::t('ui', 'Dashboard');
//$this->registerJsFile('/js/confirm-modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);

?>


<div class="hidden">
    <div id="message-1"><?= Yii::t('app', 'If you continue the operation, you will have a negative balance!') ?></div>
    <div id="message-2"><?= Yii::t('app', 'You have a negative balance!') ?></div>
</div>


<div class="row">
    <div class="col-lg-4 col-md-6 col-xs-12">
        <div style="margin-bottom: 20px;" class="small-box ">
            <div class="card-content cyan white-text">
                <div style="padding: 10px">
                    <span style="font-size: 38px"><?= $balance . $default_currency ?></span>
                    <p><?= Yii::t('ui', 'Balance') ?></p>
                </div>
                <div style="position: absolute; top: 10px; right: 25px" class="icon">
                    <i class="material-icons background-round mt-5">attach_money</i>
                </div>
                <div class="card-action cyan darken-1">
                    <div style="text-align: center" id="clients-bar"><a style="color: white"
                                                                        href="<?= Url::to(['card/index']) ?>"
                                                                        class="center-align"><?= Yii::t('ui', 'View Details') ?>
                            <i class="fa fa-arrow-circle-right"></i></a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-xs-12">
        <div style="margin-bottom: 20px;" class="small-box ">
            <div class="card-content red accent-2 white-text">
                <div style="padding: 10px">
                    <span style="font-size: 38px"><?= $spent . $default_currency ?></span>
                    <p><?= Yii::t('ui', 'Spent This Month') ?></p>
                </div>
                <div style="position: absolute; top: 10px; right: 25px" class="icon">
                    <i class="material-icons background-round mt-5">arrow_upward</i>
                </div>
                <div class="card-action red darken-1">
                    <div style="text-align: center" id="clients-bar"><a style="color: white"
                                                                        href="<?= Url::to(['operation/index', 'OperationSearch[operation]' => 'out', 'OperationSearch[date_from]' => date("Y-m-") . "01"]) ?>"
                                                                        class="center-align"><?= Yii::t('ui', 'View Details') ?>
                            <i class="fa fa-arrow-circle-right"></i></a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-xs-12">
        <div style="margin-bottom: 20px;" class="small-box ">
            <div class="card-content teal accent-4 white-text">
                <div style="padding: 10px">
                    <span style="font-size: 38px"><?= $received . $default_currency ?></span>
                    <p><?= Yii::t('ui', 'Received This Month') ?></p>
                </div>
                <div style="position: absolute; top: 10px; right: 25px" class="icon">
                    <i class="material-icons background-round mt-5">arrow_downward</i>
                </div>
                <div class="card-action teal darken-1">
                    <div style="text-align: center" id="clients-bar"><a style="color: white"
                                                                        href="<?= Url::to(['operation/index', 'OperationSearch[operation]' => 'in', 'OperationSearch[date_from]' => date("Y-m-") . "01"]) ?>"
                                                                        class="center-align"><?= Yii::t('ui', 'View Details') ?>
                            <i class="fa fa-arrow-circle-right"></i></a></div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <div style="margin-bottom: 20px;" class="small-box ">
            <div class="operation-index">
                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                    <li style="margin: 15px 0">
                        <div class="collapsible-header active">
                            <span class="box-title"><i class="fa fa-history"></i> <?= Yii::t('ui', 'Recent Operations') ?></span>
                        </div>
                        <div  class="collapsible-body">
                            <?php if (count($history) == 0) : ?>
                                <p><?= Yii::t('ui', 'There is no Operations found today.') ?></p>
                            <?php else : ?>
                                <div id="page-wrapper">
                                    <table class="table table-bordered table-main-operations table-striped table-booking-history mobile-friendly-table">
                                        <thead>
                                        <tr>
                                            <th><?= Yii::t('ui', 'Time') ?></th>
                                            <th><?= Yii::t('ui', 'From') ?></th>
                                            <th><?= Yii::t('ui', 'To') ?></th>
                                            <th><?= Yii::t('ui', 'Amount') ?></th>
                                            <th><?= Yii::t('ui', 'Comment') ?></th>
                                            <th><?= Yii::t('ui', 'Actions') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($history as $item) : ?>
                                            <?php
                                            $class = '';
                                            switch ($item->operation) {
                                                case Operation::OPERATOR_IN :
                                                    $class = 'text-success';
                                                    break;

                                                case Operation::OPERATOR_OUT :
                                                    $class = 'text-danger';
                                                    break;

                                                case Operation::OPERATOR_BETWEEN :
                                                    $class = 'text-info';
                                                    break;

                                                default:
                                                    $class = 'text-danger';
                                                    break;
                                            }
                                            ?>
                                            <tr class="<?= $class ?>">
                                                <td data-title="<?= Yii::t('ui', 'Time') ?>">
                                                    <?php echo
//                                    date("d/m/Y", strtotime($item->updated_at))."<br />".
                                                    date("H:i:s", strtotime($item->updated_at)) ?>
                                                </td>
                                                <td data-title="<?= Yii::t('ui', 'From') ?>">
                                                    <?php
                                                    switch ($item->from_type) {
                                                        case 'card' :
                                                            if(Card::find()->where(['id' => $item->from_id])->exists()){
                                                                $from = Card::find()->where(['id' => $item->from_id])->one();
                                                                $title = $from->title;
                                                            }else{
                                                                $title = '';
                                                            }
                                                            break;

                                                        case 'category' :
                                                            if(Category::find()->where(['id' => $item->from_id])->exists()){
                                                                $from = Category::find()->where(['id' => $item->from_id])->one();
                                                                $title = $from->title;
                                                            }else{
                                                                $title = '';
                                                            }
                                                            break;

                                                        case 'resource' :
                                                            if(Resource::find()->where(['id' => $item->from_id])->exists()){
                                                                $from = Resource::find()->where(['id' => $item->from_id])->one();
                                                                $title = $from->title;
                                                            }else{
                                                                $title = '';
                                                            }
                                                            break;
                                                    }
                                                    echo $title;
                                                    ?>
                                                </td>
                                                <td data-title="<?= Yii::t('ui', 'To') ?>">
                                                    <?php
                                                    switch ($item->to_type) {
                                                        case 'card' :
                                                            if(Card::find()->where(['id' => $item->to_id])->exists()){
                                                                $from = Card::find()->where(['id' => $item->to_id])->one();
                                                                $title = $from->title;
                                                            }else{
                                                                $title = '';
                                                            }
                                                            break;

                                                        case 'category' :
                                                            if(Category::find()->where(['id' => $item->to_id])->exists()){
                                                                $from = Category::find()->where(['id' => $item->to_id])->one();
                                                                $title = $from->title;
                                                            }else{
                                                                $title = '';
                                                            }
                                                            break;

                                                        case 'resource' :
                                                            if(Resource::find()->where(['id' => $item->from_id])->exists()){
                                                                $from = Resource::find()->where(['id' => $item->to_id])->one();
                                                                $title = $from->title;
                                                            }else{
                                                                $title = '';
                                                            }
                                                            break;
                                                    }
                                                    echo $title;
                                                    ?>
                                                </td>
                                                <td data-title="<?= Yii::t('ui', 'Amount') ?>">
                                                    <?= number_format($item->amount, 2, '.', ' ') . $default_currency ?>
                                                </td>
                                                <td data-title="<?= Yii::t('ui', 'Comment') ?>">
                                                    <?= empty($item->comments) ? "-" : $item->comments ?>
                                                </td>
                                                <td data-title="<?= Yii::t('ui', 'Actions') ?>">
                                                    <a href="<?= Url::to(['/operation/update', 'id' => $item->id]) ?>"
                                                       aria-label="<?= Yii::t('ui', 'Update') ?>" title="<?= Yii::t('ui', 'Update') ?>">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </a>

                                                    <?= Html::a(Yii::t('app', ' <span class="glyphicon glyphicon-trash"></span>'), ['/operation/delete', 'id' => $item->id], [
                                                        'data' => [
                                                            //'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                                            'method' => 'post',
                                                        ],
                                                    ]) ?>


                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                            <div class="box-footer clearfix">
                                <p class="pull-right"><?= Html::a(Yii::t('ui', 'View All &raquo;'), ['operation/index']) ?></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>

    <div class="col-md-6">

        <div class="operation-index">
            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                <li style="margin: 15px 0">
                    <div class="collapsible-header active">
                        <span class="box-title"><i class="fa fa-credit-card"></i> <?= Yii::t('ui', 'Cards') ?></span>
                    </div>
                    <div class="collapsible-body">
                        <?php if (count($cardList) < 1) : ?>
                            <p><?= Yii::t('ui', 'There is no Cards to display with Total > 0.00 yet! ') ?> <?= yii\helpers\Html::a("See All Cards", ['card/index']) ?></p>
                        <?php else : ?>
                            <div id="page-wrapper">
                                <table class="table table-bordered table-striped table-booking-history mobile-friendly-table">
                                    <thead>
                                    <tr>
                                        <th><?= Yii::t('ui', 'Card') ?></th>
                                        <th><?= Yii::t('ui', 'Total') ?></th>
                                        <th><?= Yii::t('ui', 'Action') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($cardList as $id => $cardItem) : ?>

                                        <tr>
                                            <td data-title="<?= Yii::t('ui', 'Card') ?>">
                                                <?= $cardItem['title'] ?>
                                            </td>
                                            <td data-title="<?= Yii::t('ui', 'Total') ?>">
                                                <?= $cardItem['total'] ?>
                                            </td>
                                            <td data-title="<?= Yii::t('ui', 'Action') ?>">
                                                <?= Html::a("<span class='glyphicon glyphicon-eye-open'></span>", ['/operation/index', 'card' => $id]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                        <?php if (count($cardList) > 0) : ?>
                            <div class="box-footer clearfix">
                                <p class="pull-right"><?= Html::a(Yii::t('ui', 'View All &raquo;'), ['card/index']) ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </li>
            </ul>
        </div>

        <div class="operation-index">
            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                <li style="margin: 15px 0">
                    <div class="collapsible-header active">
                        <span class="box-title"><i class="fa fa-bar-chart-o"></i> <?= Yii::t('ui', 'Monthly Statistic') ?></span>
                    </div>
                    <div class="collapsible-body">
                        <?php if (count($categoryStatistic) == 0) : ?>
                            <p><?= Yii::t('ui', 'There is no Operations yet! ') ?></p>
                        <?php else : ?>
                            <div id="page-wrapper">
                                <table class="table table-bordered table-striped table-booking-history mobile-friendly-table">
                                    <thead>
                                    <tr>
                                        <th><?= Yii::t('ui', 'Category') ?></th>
                                        <th><?= Yii::t('ui', 'Spent') ?></th>
                                        <th><?= Yii::t('ui', 'Action') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($categoryStatistic as $key => $value) : ?>
                                        <tr>
                                            <td data-title="<?= Yii::t('ui', 'Category') ?>">
                                                <?= Html::a($value['title'], ['operation/index',
                                                    'OperationSearch[operation]' => 'out',
                                                    'OperationSearch[to_id]' => $key,
                                                    'OperationSearch[date_from]' => date("Y-m-") . "01"]) ?>
                                            </td>
                                            <td data-title="<?= Yii::t('ui', 'Spent') ?>">
                                                <?= $value['amount'] ?>
                                            </td>
                                            <td data-title="<?= Yii::t('ui', 'Action') ?>">
                                                <?= Html::a("<span class='glyphicon glyphicon-eye-open'></span>", ['operation/index',
                                                    'OperationSearch[operation]' => 'out',
                                                    'OperationSearch[to_id]' => $key,
                                                    'OperationSearch[date_from]' => date("Y-m-") . "01"]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <p><?= Yii::t('ui', 'Total: ') ?><b><?= $spent . $default_currency ?></b></p>
                        <?php endif; ?>
                        <?php if (count($categoryStatistic) == 0) : ?>
                            <div class="box-footer clearfix">
                                <p class="pull-right"><?= yii\helpers\Html::a("View All &raquo;", ['operation/index']) ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
