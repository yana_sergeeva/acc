<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('ui', 'Help');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">
    <p>
        <?= Yii::t('ui', 'If you have any questions, please fill out the following form to contact us. Thank you.') ?>
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['readonly' => !Yii::$app->user->isGuest]) ?>

                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

                <?= $form->field($model, 'reCaptcha')->widget( \himiklab\yii2\recaptcha\ReCaptcha::className(),
                    [ 'siteKey' => '6Lf1XEQUAAAAANoZBu77OPjHos-x1jdcW4JzsvYI'
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('ui', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
        </div>
    </div>
</div>
