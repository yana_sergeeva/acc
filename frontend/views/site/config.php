<?php

/* @var $this yii\web\View */
/* @var $usersDataProvider yii\web\View ActiveDataProvider*/

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

//use Yii;

$this->title = Yii::t('ui', 'Configure');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(  '/js/confirm-modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'title')->textInput() ?>
        <?= $form->field($model, 'default_currency_id')->dropDownList($currencies,['class'=>'input-field']) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-md-6 mobile-friendly-table">
        <?= GridView::widget([
            'dataProvider' => $usersDataProvider,

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'username',
                ],
                [
                    'attribute' => 'email',
                ],
                [
                    'attribute' => 'last_login_at',
                    'format' => 'datetime'
                ],
                [
                    'label' => 'Role',
                    'value'=>function($item) use ($model){
                        return $item->getRole($model->id);
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'visible'=>$model->owner_id == Yii::$app->user->id,
                    'template' => '{delete}',
                    'buttons'=>[
                        'delete'=>function($url, $user) use ($model){

                            return $user->id==$model->owner_id?'':Html::a("<span class=\"glyphicon glyphicon-trash\"></span>",
                                   Url::to(['/account/delete-user', 'accountId'=> $model->id, 'userId'=>$user->id]),
                                   ['data-confirm'=>Yii::t('ui', 'Are you sure you want to delete this user from this account?')]);
                        }
                    ]
                ],
            ],
        ]); ?>
        <?php if($model->owner_id == Yii::$app->user->id):?>
            <?=Html::a(Yii::t('ui', 'Add user'), '#',
                [
                    'class'=>'btn btn-success',
                    'data-toggle'=>'modal',
                    'data-target'=>'#add-user-modal'
                ])?>
        <?php endif;?>
    </div>
</div>

<!-- modal -->
<div id="add-user-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; visibility: hidden">
    <div style="visibility: hidden" class="modal-dialog">
        <div style="visibility: visible" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?= Yii::t('ui', 'Invite user') ?></h4>
            </div>
            <div class="modal-body">

               <form action="<?=Url::to(['/account/add-user'])?>">
                   <div class="form-group">
                       <label for="user-email"><?=Yii::t('ui', 'Email')?></label>
                       <input id="account-id" type="hidden" name="accountId" value="<?=$model->id?>">
                       <input id="user-email" class="form-control" type="email" name="email">
                        <br>
                       <label for="user-role"><?=Yii::t('ui', 'Role')?></label>
                       <?=Html::dropDownList('role', '', ['admin'=>'admin','user'=>'user'],['name'=>'role', 'id'=>'user-role', 'class'=>'input-field'])?>
                   </div>

                   <div class="form-group">
                       <input class="btn btn-success" type="submit">
                   </div>
               </form>


             </div>
        </div>
    </div>
</div> <!-- /.modal -->
