<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = Yii::t('ui', 'MultiWallet').": ". Yii::t('ui', 'Free Simple Application for Home Financial Accounting.');
$description = Yii::t('ui', "Record your Expenses, Incomes, Money moves between cards. "
        . "See expenses by Categories. Follow financial history. "
        . "Multiple users. It's possible to use one account by several people.");

Yii::$app->view->registerMetaTag([
            'content' => $description,
            'name'    => "description"]);
Yii::$app->view->registerMetaTag([
    'content' => $description,
    'name'    => "og:description"]);

?>
<div class="site-index">
    <h4><?= Yii::t('ui', 'Features') ?></h4>
        <ol>
            <li><?= Yii::t('ui', 'Record your Expenses, Incomes, Money moves between cards.') ?></li>
            <li><?= Yii::t('ui', 'See expenses by Categories. You will know where money goes (Food, Car, House, etc.).') ?></li>
            <li><?= Yii::t('ui', 'Follow financial history. Search and sort will help.') ?></li>
            <li><?= Yii::t('ui', 'Multiple users. It\'s possible to use one account by several people.') ?></li>
        </ol>


        <p>
            <a class="btn waves-effect waves-light" href="<?=Url::to(['/user/registration/register'])?>"><i class="fa fa-user"></i> <?= Yii::t('ui', 'Register') ?></a>
            <a class="waves-effect waves-light btn pink" href="<?=Url::to(['/user/security/login'])?>"><i class="fa fa-sign-in"></i> <?= Yii::t('ui', 'Login') ?></a>
        </p>

<br />
        <p><?= Yii::t('ui', 'Service was developed by') ?> <a href="http://jdev.com.ua?utm_source=acc&utm_medium=referral" target="_blank">JDev</a> <?= Yii::t('ui', 'team') ?>.
            <br /><?= Yii::t('ui', 'We created it for our own needs and share it for anyone wants to use. It\'s Beta and it\'s <b>Free</b>*.') ?></p>
        <p><?= Yii::t('ui', '*For anyone who register and help to grow this application (find a bug, ask for feature, etc.) while it\'s Beta version - it will be Free forever.') ?></p>
        <p><?= Yii::t('ui', 'If you have any questios or proposals contact us by Email:') ?> <a href="mail:support@jdev.com.ua">support@jdev.com.ua</a></p>
        <p>
            <a class="btn waves-effect waves-light" href="/android/acc.temposys.name.apk" target="_blank"><i class="fa fa-android"></i> <?= Yii::t('ui', 'Android Application') ?></a>
        </p>
        <!--<a href="#" onclick="kuoll('createIssue', null, 'Problem description'); return false;">тест</a>-->
</div>
