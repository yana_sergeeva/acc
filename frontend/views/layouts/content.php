<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

$this->registerJsFile('/materialize/js/scripts/advanced-ui-modals.js');
?>

<section style="margin-bottom: 20px" class="content-header">
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l6">
                    <?php if (isset($this->blocks['content-header'])) { ?>
                        <h5  class="breadcrumbs-title breadcrumbs-style"><?= $this->blocks['content-header'] ?></h5>
                    <?php } else { ?>
                        <h5 class="breadcrumbs-title breadcrumbs-style">
                            <?php
                            if ($this->title !== null) {
                                echo \yii\helpers\Html::encode($this->title);
                            } else {
                                echo \yii\helpers\Inflector::camel2words(
                                    \yii\helpers\Inflector::id2camel($this->context->module->id)
                                );
                                echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                            } ?>
                        </h5>
                    <?php } ?>
                    <?=
                    Breadcrumbs::widget(
                        [
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]
                    ) ?>

                </div>
            </div>
        </div>
    </div>
</section>

<div style="width: 98%" class="container">
    <div id="page-wrapper" class="content-wrapper">


        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>

    <?php if(!Yii::$app->user->isGuest):?>
    <!-- Control Sidebar -->
        <div id="side_toggle" class="fixed-action-btn" style="bottom: 50px; right: 19px;z-index: 2!important;">
            <a style="background-color: #ff4081" class="btn-floating btn-large">
                <i class="material-icons">add</i>
            </a>
        </div>

        <aside id="right-sidebar-nav">
            <div style="z-index: 3; transform: translateX(100%);" id="chat-out" class="side-nav rightside-navigation right-aligned ps-container ps-active-y">
                <?=\frontend\widgets\MoneyOperationsWidget::widget()?>
                <div class="ps-scrollbar-x-rail in-scrolling" style="left: 0px; bottom: 3px;">
                    <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>     
                </div>
                <div class="ps-scrollbar-y-rail" style="top: 0px; height: 501px; right: 3px;">
                    <div class="ps-scrollbar-y" style="top: 0px; height: 261px;"></div>
                </div>
            </div>
        </aside>


    <?php endif;?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class='control-sidebar-bg'></div>
</div>