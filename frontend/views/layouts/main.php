<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use lajax\translatemanager\bundles\TranslateManagerAsset;

//\macgyer\yii2materializecss\assets\MaterializeAsset::register($this);
AppAsset::register($this);
\frontend\assets\MaterialAsset::register($this);
TranslateManagerAsset::register($this);
//dmstr\web\AdminLteAsset::register($this);
//\macgyer\yii2materializecss\assets\MaterializeAsset::register($this);
$this->registerJsFile('/js/operations.js', ['position' => \yii\web\View::POS_END, 'depends'=> '\yii\web\JqueryAsset']);


$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/logo.png"/>
    <?php $this->head() ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-80186160-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body>
<?php $this->beginBody() ?>

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<div class="alert-danger alert fade in"  style="width: 250px; height: 100px; position: fixed; margin-left: auto;margin-right: auto;left: 0;right: 0; display: none; z-index: 900" id="notification"></div>


<?= $this->render(
    'header.php',
    ['directoryAsset' => $directoryAsset]
) ?>


<div id="main" >
<div class="wrapper">

    <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset]
    )
    ?>

    <section id="content">
        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>
    </section>
</div>
</div>
<div id="ajax-url" style="visibility: hidden"><?php echo Url::to(['/operation/add-operation']) ?></div>
<footer style = "background-color: #00bcd4;margin: 0" class="page-footer">
    <div class="footer-copyright">
        <div style="text-align: center; color: white" class="container">
            <span >
                <strong>Copyright © 2016-<?=date("Y");?> <a href="http://jdev.com.ua"><span id="footer_link" style="color: white;font-weight: bold">JDev</span></a>.</strong> All rights reserved.
            </span>
        </div>
    </div>
</footer>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>




























