<?php
use common\models\FinAccount;
use common\models\UserAccount;
use frontend\widgets\CustomLanguagePicker;
use lajax\translatemanager\models\Language;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
$this->registerCssFile('/css/site1.css');
$currentLanguage = Language::find()->where(['like', 'language_id', Yii::$app->language])->one();
$this->registerJsFile('/js/menu.js', ['position' => \yii\web\View::POS_END, 'depends'=> '\yii\web\JqueryAsset']);
?>


<header id="header" class="page-topbar">
    <div class="navbar-fixed">
        <nav style="background-color: #00bcd4" class="navbar-color">
            <div class="nav-wrapper">
                <h1 class="logo-wrapper">
                    <a href="/" class="brand-logo cyan">
                        <img style="height: 40px" src="/logo.png" alt="<?= Yii::$app->name ?>">
                    <span  class=" logo-text hide-on-med-and-down"><?= Yii::$app->name ?></span>
                    </a>
                </h1>

                <a href="#" class="sidebar-toggle" data-activates="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                    <ul class="right hide-on-med-and-down1">

                        <li>
                            <a id="first_drop" href="javascript:void(0);"
                               class="waves-effect waves-block waves-light translation-button"
                               data-activates="translation-dropdown">

                                <span class="flag-icon flag-icon-<?= $currentLanguage->country ?? '' ?>"></span><span class="language_hide" style="margin-left: 10px"><?=$currentLanguage->name ?? null?></span>
                            </a>
                        </li>
                        <li>
                            <ul id="translation-dropdown" class="dropdown-content">
                                <?= CustomLanguagePicker::widget([
                                    'itemTemplate' => '<li><a href="{link}" class="grey-text text-darken-1 qwe"><i class="flag-icon flag-icon-{country}"></i> {name}</a></li>',
                                    'activeItemTemplate' => '<li><a href="{link}" class="grey-text text-darken-1 123"><i class="flag-icon flag-icon-{country}"></i> {name}</a></li>',
                                    'parentTemplate' => '{items}',
                                    'languageAsset' => 'lajax\languagepicker\bundles\LanguageLargeIconsAsset',      // StyleSheets
                                    'languagePluginAsset' => 'lajax\languagepicker\bundles\LanguagePluginAsset',    // JavaScripts
                                ]); ?>
                            </ul>
                        </li>
                       

                        <?php if (!Yii::$app->user->isGuest) { ?>
                        <li>
                            <a id="second_drop" href="javascript:void(0);"
                               class="waves-effect waves-block waves-light notification-button"
                               data-activates="notifications-dropdown">
                                <i class="fa fa-briefcase"></i>
                                <?php $title = FinAccount::findone(UserAccount::getFinId())->title ?>
                                <span class="hidden-xs"><?=Yii::t('ui', $title)?> <i class="fa fa-caret-down"></i></span>
                            </a>
                            <ul style="position:absolute;z-index: 999" id="notifications-dropdown" class="dropdown-content">
                                <li ><a class="grey-text text-darken-1" href="/config"><i
                                                class="fa fa-cog"></i> <?=Yii::t('ui','Configure')?></a></li>
                                <li ><a class="grey-text text-darken-1" href="/account/create"><i
                                                class="fa fa-plus"></i> <?=Yii::t('ui','Create Account')?></a></li>
                                    <?php $account = FinAccount::find()->where(
                                        ['IN', 'id',
                                            ArrayHelper::map(
                                                UserAccount::findAll(['user_id'=>Yii::$app->user->id]),
                                                'account_id',
                                                'account_id')
                                        ]) ?>

                                    <?php foreach ($account->all() as $acc) { ?>
                                        <li>
                                            <?php if(UserAccount::getFinId() == $acc->id) { ?>
                                                <a style="color: purple!important;" class="grey-text text-darken-1" href="<?="/account/change-account/" . $acc->id?>">
                                                <?php $title = $acc->title ?>
                                                <i style="color: purple" class="fa fa-check"></i><?=Yii::t('ui',$title)?></a>
                                            <?php }else{ ?>
                                                <a class="grey-text text-darken-1" href="<?="/account/change-account/" . $acc->id?>">
                                                <?php $title = $acc->title ?>
                                                <i class="fa fa-check"></i> <?=Yii::t('ui',$title)?></a>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                            </ul>
                        <?php } ?>


                    <?php if (!Yii::$app->user->isGuest) { ?>
                        <li>
                            <a id="third_drop" href="javascript:void(0);"
                               class="waves-effect waves-block waves-light notification-button"
                               data-activates="notifications-dropdown1">
                                <i class="material-icons">face</i> 
                                <?php // if (!Yii::$app->user->isGuest) { ?>
<!--                                    <i class="fa fa-user fa-fw"></i><span
                                            class="hidden-xs"> <?= Yii::$app->user->identity->username ?> <i
                                                class="fa fa-caret-down"></i></span>-->
                                <?php // } else { ?>
<!--                                    <i class="fa fa-user fa-fw"></i><span class="hidden-xs"> <?=Yii::t('ui','User')?> <i
                                                class="fa fa-caret-down"></i></span>-->
                                <?php // } ?>
                            </a>
                            <ul class="dropdown-content" style="z-index: 999;position: relative!important;" id="notifications-dropdown1">
                                <li >
                                    <a class="grey-text text-darken-1" href="<?= Url::to(['/user/settings/account']) ?>"><i 
                                            class="material-icons">face</i> <?=Yii::t('ui','Profile')?></a>
                                </li>
                                <li ><a class="grey-text text-darken-1" href="<?= Url::to(['/account/index']) ?>"><i 
                                            class="material-icons">card_travel</i> <?=Yii::t('ui','Accounts')?></a></li>
                                <li class="divider"></li>               
                                <li ><a class="grey-text text-darken-1" href="<?= Url::to(['/user/logout']) ?>"
                                       data-method="post"><i class="material-icons">keyboard_tab</i> <?=Yii::t('ui','Logout')?></a></li>
                            </ul>
                    <?php } ?>

                    <?php if(Yii::$app->user->isGuest){ ?>
                        <li>
                            <a href="<?= Url::to(['/user/register']) ?>">
                                <i class="fa fa-user fa-fw"></i><span class="language_hide">Register</span>                     </a>
                        </li>
                    <?php } ?>

                    <?php if(Yii::$app->user->isGuest){ ?>
                        <li>
                            <a href="<?= Url::to(['/user/login']) ?>">
                                <i class="fa fa-sign-in fa-fw"></i><span class="language_hide">Login</span>                     </a>
                        </li>
                    <?php } ?>

                    </ul>
                </div>
        </nav>
    </div>
</header>

