<?php
use yii\helpers\Html;
use yii\helpers\Url;
$user = \common\models\User::findOne(Yii::$app->user->id);
$name = (isset($user->profile->name) && $user->profile->name) ? $user->profile->name : '';
?>

<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y">
    <li class="no-padding">
        <ul class="collapsible" data-collapsible="accordion">
            <?php if (Yii::$app->user->isGuest) { ?>
                <li class="bold">
                    <a href="/" class="waves-effect waves-cyan">
                        <i class="material-icons">home</i>
                        <span class="nav-text"><?=Yii::t('ui','Home')?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <li class="user-details cyan darken-2">
              <div class="row">
<!--                <div class="col col s4 m4 l4">
                  <img src="../../images/avatar/avatar-10.png" alt=""
                       class="circle responsive-img valign profile-image cyan">
                </div>-->
                <div class="col col s12 m12 l12">

                  <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"
                     href="#" data-activates="profile-dropdown-nav"><?=  $name ? $name : Yii::$app->user->identity->username ?><i class="mdi-navigation-arrow-drop-down right"></i></a><ul id="profile-dropdown-nav" class="dropdown-content">
                    <li>
                      <a href="<?= Url::to(['/user/settings/account']) ?>" class="grey-text text-darken-1">
                        <i class="material-icons">face</i> <?=Yii::t('ui','Profile')?></a>
                    </li>
                    <li>
                      <a href="<?= Url::to(['/account/index']) ?>" class="grey-text text-darken-1">
                        <i class="material-icons">card_travel</i> <?=Yii::t('ui','Accounts')?></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                      <a href="<?= Url::to(['/user/logout']) ?>" class="grey-text text-darken-1" data-method="post">
                        <i class="material-icons">keyboard_tab</i> <?=Yii::t('ui','Logout')?></a>
                    </li>
                  </ul>
                  <p class="user-roal"><?= Yii::$app->user->identity->email ?></p>
                </div>
              </div>
            </li>
                <li class="bold">
                    <a href="<?= Yii::$app->homeUrl ?>" class="waves-effect waves-cyan">
                        <i class="material-icons">dashboard</i>
                        <span class="nav-text"><?=Yii::t('ui','Dashboard')?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <li class="bold">
                    <a href="<?= Url::to(['/operation/index']) ?>" class="waves-effect waves-cyan">
                        <i class="material-icons">history</i>
                        <span class="nav-text"><?=Yii::t('ui','History')?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <li class="bold">
                    <a href="<?= Url::to(['/statistic/index']) ?>" class="waves-effect waves-cyan">
                        <i class="material-icons">insert_chart</i>
                        <span class="nav-text"><?=Yii::t('ui','Statistic')?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">build</i>
                        <span class="nav-text"><?=Yii::t('ui','Settings')?></span>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <a href="<?= Url::to(['/card/index']) ?>">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <i class="material-icons">credit_card</i>
                                    <span><?=Yii::t('ui','Cards')?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['/category/index']) ?>">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <i class="material-icons">apps</i>
                                    <span><?=Yii::t('ui','Categories')?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['/resource/index']) ?>">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <i class="material-icons">arrow_drop_down_circle</i>
                                    <span><?=Yii::t('ui','Resources')?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            <?php } ?>

            <li class="bold">
                <a href="<?= Url::to('/site/contact') ?>" class="waves-effect waves-cyan">
                    <i class="material-icons">mail_outline</i>
                    <span class="nav-text"><?=Yii::t('ui','Help')?></span>
                </a>
            </li>
            <li class="bold">
                <a href='/android/acc.temposys.name.apk' class="waves-effect waves-cyan">
                    <i class="material-icons">android</i>
                    <span class="nav-text"><?=Yii::t('ui','Android Application')?></span>
                </a>
            </li>


        </ul>
        </ul>


    <a style="background-color: #ff4081; z-index: 999" href="#" data-activates="slide-out"
       class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
        <i class="material-icons">menu</i>
    </a>
</aside>

