<?php

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = Yii::t('app', 'Main page');

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="/logo.png"/>
        <?php $this->head() ?>

        <!-- Startuply favicon -->
        <link rel="shortcut icon" href="/logo.png">

        <!-- Wordpress head functions -->
        <title>Multiwallet</title>



        <script type="text/javascript">
            window._wpemojiSettings = {
                "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/",
                "ext": ".png",
                "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/",
                "svgExt": ".svg",
                "source": {"concatemoji": "http:\/\/demo.startuplywp.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.3"}
            };
            !function (a, b, c) {
                function d(a) {
                    var b, c, d, e, f = String.fromCharCode;
                    if (!k || !k.fillText)return !1;
                    switch (k.clearRect(0, 0, j.width, j.height), k.textBaseline = "top", k.font = "600 32px Arial", a) {
                        case"flag":
                            return k.fillText(f(55356, 56826, 55356, 56819), 0, 0), !(j.toDataURL().length < 3e3) && (k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57331, 55356, 57096), 0, 0), c = j.toDataURL(), b !== c);
                        case"emoji4":
                            return k.fillText(f(55357, 56425, 55356, 57341, 8205, 55357, 56507), 0, 0), d = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55357, 56425, 55356, 57341, 55357, 56507), 0, 0), e = j.toDataURL(), d !== e
                    }
                    return !1
                }

                function e(a) {
                    var c = b.createElement("script");
                    c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
                }

                var f, g, h, i, j = b.createElement("canvas"), k = j.getContext && j.getContext("2d");
                for (i = Array("flag", "emoji4"), c.supports = {
                    everything: !0,
                    everythingExceptFlag: !0
                }, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
                c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                    c.DOMReady = !0
                }, c.supports.everything || (g = function () {
                    c.readyCallback()
                }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
                    "complete" === b.readyState && c.readyCallback()
                })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
            }(window, document, window._wpemojiSettings);
        </script>

        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>

        <link rel="stylesheet" href="/fonts/startuply/fontawesome-webfont.woff2" type="woff2">
        <link rel="stylesheet" href="/fonts/startuply/line-icons.ttf" type="application/font-sfnt">
        <link rel="stylesheet" href="/fonts/startuply/line-icons.woff" type="woff">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/fonts/startuply/fontawesome-webfont.woff" type="woff">
        <link rel="stylesheet" href="/fonts/startuply/fontawesome-webfont.ttf" type="application/font-sfnt">
        <link rel="stylesheet" href="/fonts/startuply/fontawesome-webfont1.woff2" type="woff2">
        <link rel="stylesheet" href="/fonts/startuply/fontawesome-webfont1.ttf" type="application/font-sfnt">

        <link rel='stylesheet' id='startuply_lineicons_1-css'
              href='/css/Startup/font-lineicons.css?ver=3.0'
              type='text/css' media='screen'/>



        <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1458900249119 {
                padding-top: 150px;
                padding-bottom: 150px;
            }

            .vc_custom_1459326738413 {
                padding-top: 20px;
                padding-bottom: 20px;
            }

            .vc_custom_1424378068484 {
                padding-top: 50px;
            }

            .vc_custom_1423845105581 {
                padding-top: 15px;
            }

            .vc_custom_1427199365504 {
                padding-top: 10px;
                padding-bottom: 25px;
            }

            .vc_custom_1423845168912 {
                padding-top: 110px;
                padding-bottom: 35px;
            }

            .vc_custom_1424792145739 {
                padding-top: 50px;
                padding-bottom: 30px;
            }

            .vc_custom_1489794819977 {
                padding-top: 5px !important;
                padding-bottom: 10px !important;
            }

            .vc_custom_1429533656107 {
                padding-top: 20px;
            }

            .vc_custom_1429533701919 {
                padding-top: 10px;
            }

            .vc_custom_1492558688385 {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
            }

            .vc_custom_1492558708372 {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
            }

            .vc_custom_1492558747031 {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
            }

            .vc_custom_1492559882054 {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
            }

            .vc_custom_1492559889835 {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
            }

            .vc_custom_1492558851283 {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
            }</style>
        <noscript>
            <style type="text/css"> .wpb_animate_when_almost_visible {
                    opacity: 1;
                }</style>
        </noscript>
    </head>

    <body id='landing-page' class="landing-page">
    <?php $this->beginBody() ?>

    <?= $this->render(
        '/landing/index.php'
    ) ?>


    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>