<?php
/* @var $this yii\web\View */
/* @var $filterForm StatisticFilterForm*/

use common\models\StatisticFilterForm;
use dosamigos\chartjs\ChartJs;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

$this->title = Yii::t('ui', 'Statistics');
?>

<div>
    <?php $form = ActiveForm::begin(['id' => 'join-forms']); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                    <li style="margin: 0" class="active">
                        <div class="collapsible-header active">
                            <span style="font-size: 18px" class="box-title"><i class="fa fa-bar-chart-o"></i> <?= Yii::t('ui', 'Monthly Expenses') ?></span>
                        </div>
                        <div class="collapsible-body collapse" >
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php echo $form->field($filterForm, 'date')->widget(DatePicker::className(), [
                                                    'language'   => Yii::$app->language,
                                                    'pluginOptions' => [
                                                        'autoclose' => true,
                                                        'startView'=>'year',
                                                        'minViewMode'=>'months',
                                                        'format' => 'mm.yyyy'
                                                    ]
                                                ])->label(Yii::t('ui', "Date"),['style'=>'color: black']) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-form-submit']) ?>
                                                </div>



                                                <?php if (count($categoryStatistic) == 0) : ?>
                                                    <p><?= Yii::t('ui', 'There is no Operations for this period! ') ?></p>
                                                <?php else : ?>
                                                    <div id="page-wrapper">
                                                        <table class="table table-bordered table-striped table-booking-history mobile-friendly-table">
                                                            <thead>
                                                            <tr>
                                                                <th><?= Yii::t('ui', 'Category') ?></th>
                                                                <th><?= Yii::t('ui', 'Spent') ?></th>
                                                                <th>%</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach($categoryStatistic as $key => $value) : ?>
                                                                <tr>
                                                                    <td data-title="<?= Yii::t('ui', 'Category') ?>">
                                                                        <?= Html::a($value['title'], ['operation/index',
                                                                            'OperationSearch[operation]' => 'out',
                                                                            'OperationSearch[to_id]' => $key,
                                                                            'OperationSearch[date_from]' => date("Y-m-")."01"])?>
                                                                    </td>
                                                                    <td data-title="<?= Yii::t('ui', 'Spent') ?>">
                                                                        <?=$value['amount']?>
                                                                    </td>
                                                                    <?php $value['amount'] = preg_replace("/\s|&nbsp;/",'' , $value['amount']);
                                                                          $spent_changed = preg_replace("/\s|&nbsp;/",'' , $spent);
                                                                    ?>

                                                                    <td data-title="<?= Yii::t('ui', '%') ?>">
                                                                        <?= (round(floatval(str_replace('$' , '' , $value['amount'])) / $spent_changed * 100)) . '%'?>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                    </div>


                                    <div style="text-align: center" class="col col-md-6 col-xs-12">
                                        <div>
                                            <?= ChartJs::widget([
                                                'type' => 'pie',

                                                'options' => [
                                                    'id' => 'pie',
                                                    'height' => 200,
                                                    'width' => 400,
                                                    'responsive' => true,
                                                    'maintainAspectRatio' => false,
                                                ],

                                                'clientOptions' => [
                                                    'tooltips' => [
                                                        'callbacks'=> [
                                                            'label' => new JsExpression("function(tooltipItem, data) {
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                        var tooltipLabel = data.labels[tooltipItem.index];
                                                        var tooltipData = allData[tooltipItem.index];
                                                        var total = 0;
                                                        for (var i in allData) {
                                                            total += allData[i];
                                                        }
                                                        var tooltipPercentage = Math.round((tooltipData / total) * 100);
                                                        return tooltipLabel + ' : ' + tooltipData + ' (' + tooltipPercentage + '%)' } ")
                                                        ]
                                                    ],
                                                ],

                                                'data' => [
                                                    'labels' => $titles,
                                                    'datasets' => [
                                                        [
                                                            'label' => "My First dataset",
                                                            'backgroundColor' => $colors,
                                                            'data' => $sum
                                                        ],

                                                    ]
                                                ]

                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div style="float: left" class="box-footer clearfix">
                                <?php if (count($categoryStatistic) == 0) : ?>
                                    <p class="pull-right"><?= Html::a("View All &raquo;", ['operation/index'])?></p>
                                <?php else : ?>
                                    <p class="pull-right"><?= Yii::t('ui', 'Total: ') ?><b><?=$spent.$default_currency?></b></p>
                                <?php endif;?>
                            </div>
                        </div>

                    </li>
                </ul>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                    <li style="margin: 0" class="active">
                        <div class="collapsible-header active">
                            <span style="font-size: 18px" class="box-title"><i class="fa fa-bar-chart-o"></i> <?= Yii::t('ui', 'Monthly Income') ?></span>
                        </div>
                        <div class="collapsible-body collapse" >
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php echo $form->field($filterForm, 'dateTo')->widget(DatePicker::className(), [
                                                    'language'   => Yii::$app->language,
                                                    'pluginOptions' => [
                                                        'autoclose' => true,
                                                        'startView'=>'year',
                                                        'minViewMode'=>'months',
                                                        'format' => 'mm.yyyy'
                                                    ]
                                                ])->label(Yii::t('ui', "Date"),['style'=>'color: black']) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-form-submit']) ?>
                                                </div>

                                                <?php if (count($categoryStatisticEarned) == 0) : ?>
                                                    <p><?= Yii::t('ui', 'There is no Operations for this period! ') ?></p>
                                                <?php else : ?>
                                                    <div id="page-wrapper">
                                                        <table class="table table-bordered table-striped table-booking-history mobile-friendly-table">
                                                            <thead>
                                                            <tr>
                                                                <th><?= Yii::t('ui', 'Category') ?></th>
                                                                <th><?= Yii::t('ui', 'Spent') ?></th>
                                                                <th>%</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach($categoryStatisticEarned as $key => $value) : ?>
                                                                <tr>
                                                                    <td data-title="<?= Yii::t('ui', 'Category') ?>">
                                                                        <?= Html::a($value['title'], ['operation/index',
                                                                            'OperationSearch[operation]' => 'out',
                                                                            'OperationSearch[to_id]' => $key,
                                                                            'OperationSearch[date_from]' => date("Y-m-")."01"])?>
                                                                    </td>
                                                                    <td data-title="<?= Yii::t('ui', 'Spent') ?>">
                                                                        <?=$value['amount']?>
                                                                    </td>
                                                                    <?php $value['amount'] = preg_replace("/\s|&nbsp;/",'' , $value['amount']);
                                                                    $earned_changed = preg_replace("/\s|&nbsp;/",'' , $earned);
                                                                    ?>

                                                                    <td data-title="<?= Yii::t('ui', '%') ?>">
                                                                        <?= (round(floatval(str_replace('$' , '' , $value['amount'])) / $earned_changed * 100)) . '%'?>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                    </div>


                                    <div style="text-align: center" class="col col-md-6 col-xs-12">
                                        <div>
                                            <?= ChartJs::widget([
                                                'type' => 'pie',
                                                'options' => [
                                                    'id' => 'pie1',
                                                    'height' => 200,
                                                    'width' => 400,
                                                    'responsive' => true,
                                                    'maintainAspectRatio' => false,
                                                ],

                                                'clientOptions' => [
                                                    'tooltips' => [
                                                        'callbacks'=> [
                                                            'label' => new JsExpression("function(tooltipItem, data) {
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                        var tooltipLabel = data.labels[tooltipItem.index];
                                                        var tooltipData = allData[tooltipItem.index];
                                                        var total = 0;
                                                        for (var i in allData) {
                                                            total += allData[i];
                                                        }
                                                        var tooltipPercentage = Math.round((tooltipData / total) * 100);
                                                        return tooltipLabel + ' : ' + tooltipData + ' (' + tooltipPercentage + '%)' } ")
                                                        ]
                                                    ],
                                                ],


                                                'data' => [
                                                    'labels' => $titlesEarned,
                                                    'datasets' => [
                                                        [
                                                            'label' => "My First dataset",
                                                            'backgroundColor' => $colors,
                                                            'data' => $sumEarned
                                                        ],

                                                    ]
                                                ]
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="float: left" class="box-footer clearfix">
                                <?php if (count($categoryStatisticEarned) == 0) : ?>
                                    <p class="pull-right"><?= Html::a("View All &raquo;", ['operation/index'])?></p>
                                <?php else : ?>
                                    <p class="pull-right"><?= Yii::t('ui', 'Total: ') ?><b><?=$earned.$default_currency?></b></p>
                                <?php endif;?>
                            </div>
                        </div>

                    </li>
                </ul>

            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>     


