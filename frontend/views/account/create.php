<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FinAccount */
/* @var $currencies array */

$this->title = 'Create Fin Account';
$this->params['breadcrumbs'][] = ['label' => 'Fin Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fin-account-create">

    <?= $this->render('_form', [
        'model' => $model,
        'currencies' => $currencies,
    ]) ?>

</div>