<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FinAccount */

$this->title = 'Update Fin Account: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Fin Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fin-account-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>