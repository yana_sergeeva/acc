<?php
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView;

/**@var $dataProvider ActiveDataProvider*/
/**@var $currentAccountId integer*/
$this->registerJsFile('/js/change_account.js', ['depends' => [yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Account');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="account-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create Account'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ListView::widget(
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_account-item',
            'viewParams'=>
            [
                    'currentAccountId'=>$currentAccountId
            ]
        ]
    )
    ?>

</div>
