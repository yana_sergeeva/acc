<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 7/18/17
 * Time: 2:46 PM
 */
use common\models\Currency;
use common\models\FinAccount;
use common\models\User;
use common\models\UserAccount;
use yii\widgets\DetailView;

/**@var $model FinAccount*/
/**@var $currentAccountId integer*/

?>

<div class="account-item col-md-4 col-sm-6">
    <?php if($model->id == $currentAccountId):?>
        <div class="panel panel-danger">
    <?php else:?>
        <div class="panel panel-success">
    <?php endif;?>

        <div class="panel-heading">
            <?=Yii::t('app', 'Account').'  #'.$model->id?>
            <b><?=$model->title?></b>
        </div>
        <div class="panel-body ">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'owner_id',
                        'label' => Yii::t('app', 'Owner'),
                        'value' => User::findOne($model->owner_id)->username,
                        'contentOptions' => ['class' => 'show-in-line'],
                    ],
                    [
                        'attribute' => 'default_currency_id',
                        'label'=> Yii::t('app', 'Currency'),
                        'value' => function($data) {
                            $currency = Currency::findOne($data['default_currency_id']);
                            return $currency->iso_code . ' ('.$currency->symbol.')';
                        },
                        'contentOptions' => ['class' => 'show-in-line'],
                    ],
                    [
                        'label' => Yii::t('app', 'Members'),
                        'value' =>count(UserAccount::findAll(['account_id'=>$model->id])),

                    ],
                    [
                        'label' => Yii::t('app', 'Role'),
                        'value' => UserAccount::findOne(['user_id'=>Yii::$app->user->id, 'account_id'=>$model->id])->role,
                        'contentOptions' => ['class' => 'show-in-line'],
                    ],
                ],
            ]) ?>
            <div class="pull-right">
                <button data-id="<?=$model->id?>" class="btn btn-success change-acc <?=($model->id == $currentAccountId)?'disabled':''?>">
                    <?=Yii::t('app', 'Choose it')?>
                </button>
            </div>
        </div>

    </div>

</div>
