<?php

use common\models\Card;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Card */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'currency_id')->dropDownList($currencies) ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([1 => Yii::t('ui', Card::ORDINARY_CARD), 0=>Yii::t('ui', Card::CREDIT_CARD)], ['class'=>"select-dropdown"] ) ?>

    <?= $form->field($model, 'display')->dropDownList([1=> Yii::t('ui', 'ON'), 2=> Yii::t('ui', 'OFF')], ['class'=> 'input-field']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
