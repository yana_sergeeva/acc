<?php

use common\models\Card;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use common\models\Currency;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cards');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/input.css');

?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body mobile-friendly-table">
    <div id="mult-del" style="margin-bottom: 20px; float: right">
        <button style="background-color: #ff4081!important" id="submit-delete" class="btn btn-info">Delete multiple</button>
    </div>
    <p>
        <?= Html::a("<i class='fa fa-plus'></i> ".Yii::t('app', 'Create Card'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>
        <?=Html::beginForm(['card/delete-multiple'],'post', ['id' => 'form-statistic']);?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['data-title' => '#'],],

            [
                'attribute' => 'title',
                'contentOptions' => ['data-title' => Yii::t('app', 'Card')],
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    switch($model->type) {
                        case 0:
                            return Yii::t('app',Card::CREDIT_CARD);
                            break;
                        case 1:
                            return Yii::t('app',Card::ORDINARY_CARD);
                            break;
                        default:
                            return '';
                            break;
                    }
                },
                'contentOptions' => ['data-title' => Yii::t('app', 'Type')],
            ],
            [
                'attribute' => 'total',
                'contentOptions' => ['data-title' => Yii::t('app', "Total")],
                'format'             => 'html',
                'value'              =>  function($data)
                {
                    $cur = Currency::find()->where(['id' => $data->currency_id])->one();
                    return                 number_format($data->total, 2, '.', ' ').$cur->symbol;
                }
            ],
            [
                'attribute' => 'display',
                'label'     => Yii::t('app', "Display"),
                'contentOptions' => ['data-title' => Yii::t('app', "Display")],
                'format' => 'html',
                'value'=> function($data)
                {
                    return ($data->display == 1) ? Yii::t('ui', 'ON') : Yii::t('ui', 'OFF');
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                'buttons'=>[
                        'view'=>function($url, $item){
                            return Html::a("<span class='glyphicon glyphicon-eye-open'></span>", ['/operation/index', 'card' => $item->id],  ['data-pjax'=>0]);

                        }
                ]

            ],
            ['class' => 'yii\grid\CheckboxColumn',
                'header' => Html::checkBox('selection_all', false, [
                    'class' => 'select-on-check-all',
                    'label' => 'Check All',
                ]),
                'checkboxOptions' => function($model) {
                    return ['value' => $model->id, 'class' => 'filled-in'];
                },
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>

                <?= Html::endForm();?>
            </div>
        </div>
    </div>
</div>