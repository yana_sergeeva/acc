<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Card */

$this->title = Yii::t('app', 'Create Card');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
        'currencies' => $currencies,
    ]) ?>
            </div>
        </div>
    </div>
</div>
