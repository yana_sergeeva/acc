
$('#reset-form').on('click', function (e) {
    e.preventDefault;
    $("#search-history input").each(function(){
        if($(this).hasClass('select-dropdown')){
            $(this).val('All');
        }else{
            $(this).val('');
        }

    });

    $("#operationsearch-card_id").val('All').change();
    $("#operationsearch-to_id").val('All').change();
    $("#operationsearch-from_id").val('All').change();
    $("#operationsearch-operation").val('All').change();

});

$('.btn-ajax').click(function(e) {
    e.preventDefault();
    var form = $(this).parent().parent();
    var data = getFormData(form);

    $.ajax({
        type: "POST",
        url: $('#ajax-url').text(),
        data: data,
        dataType: 'json',
        success: function (result) {
            if(result == 'Success'){

                $('#notification').css("background-color","#dff0d8");
                $('#notification').css("color","green");
                $('#notification').empty().append("<span >" + result + "</span>");
                $('#w1').trigger('reset');
                $('#w2').trigger('reset');
                $('#w3').trigger('reset');
                if(window.location.pathname == '/dashboard' || window.location.pathname == '/ru/dashboard' || window.location.pathname == '/operation/index' || window.location.pathname == '/ru/operation/index'){
                    location.reload();
                    localStorage.setItem('anchor', true);
                }
            }else{
                $('#notification').css("background-color","#f2dede");
                $('#notification').css("color","#a94442");
                $('#notification').empty().append("<span >" + result + "</span>");
            }
            $("#notification").css('display','block');
            setTimeout(function() {
                $("#notification").fadeOut();
            }, 1500);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('упс');
        }
    });

});

// $(document).on('beforeSubmit', '#w1', function (e) {
//     e.preventDefault();
//     console.log(312);
// })


function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}


$('.collapsible-header.active').click(function (e) {
    $("input[name='selection[]']").toggleClass('hidden');
});

$('#submit-delete').click(function () {
    document.getElementById("form-statistic").submit();
});

