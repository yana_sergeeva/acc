/**
 * Created by dev on 29.11.17.
 */
var count = 0;

$('.waves-block').click(function(e) {
    var a = $(this).next();
    e.stopPropagation();
    if(count){

        if (a.css('display') === "block") {
            a.css("display", "none");
        }
        else {
            a.css("display", "block");
        }
    }else{
        count++;
        a.css("display", "block");
    }
});


$(document).on('click', function (e) {
    count = 0;
});