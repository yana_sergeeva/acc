function floatAmount(input) {
    var value = input.value;
    var rep = /[^0-9\.]/;
    if (rep.test(value)) {
        value = value.replace(rep, '');
        input.value = value;
    };
};


