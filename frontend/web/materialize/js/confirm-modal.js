/**
 * Created by dev on 7/13/17.
 */

$(document).ready(function() {
    yii.confirm = function (message, okCallback, cancelCallback) {

        if(message == 'add_expenses'){
            if (money[$('#in_from_id option:selected').val()] < 0) {
                showConfirm(document.getElementById("message-2").innerHTML, okCallback);
                return false;
            }
            else if ($('#in_amount').val() > money[$('#in_from_id option:selected').val()]) {
                showConfirm(document.getElementById("message-1").innerHTML, okCallback);
                return false;
            }
        }
        else if(message == 'add_move'){
            if (money[$('#btw_from_id option:selected').val()] < 0) {
                showConfirm(document.getElementById("message-2").innerHTML, okCallback);
                return false;
            }
            else if ($('#btw_amount').val() > money[$('#btw_from_id option:selected').val()]) {
                showConfirm(document.getElementById("message-1").innerHTML, okCallback);
                return false;
            }
        }
        else if(message !== ''){
            showConfirm(message, okCallback);
            return false;
        }
        else {
            if ($('#operation-in').hasClass('active') ) {
                if (money[$('#in_from_id option:selected').val()] < 0) {
                    showConfirm(document.getElementById("message-2").innerHTML, okCallback);
                    return false;
                }
                else if ($('#in_amount').val() > money[$('#in_from_id option:selected').val()]) {
                    showConfirm(document.getElementById("message-1").innerHTML, okCallback);
                    return false;
                }
            }
            else if ($('#operation-btw').hasClass('active')) {
                if (money[$('#btw_from_id option:selected').val()] < 0) {
                    showConfirm(document.getElementById("message-2").innerHTML, okCallback);
                    return false;
                }
                else if ($('#btw_amount').val() > money[$('#btw_from_id option:selected').val()]) {
                    showConfirm(document.getElementById("message-1").innerHTML, okCallback);
                    return false;
                }
            }
        }
        return okCallback();
    };
});

function showConfirm(m, okCallback) {
    var mess = document.getElementById("message");

    mess.innerHTML = m;

    $('#modal_window').modal();
    $('#modal-ok').unbind('click').click(okCallback);
}