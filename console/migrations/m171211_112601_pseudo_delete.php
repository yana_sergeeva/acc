<?php

use yii\db\Migration;

/**
 * Class m171211_112601_pseudo_delete
 */
class m171211_112601_pseudo_delete extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('card', 'is_deleted', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('resource', 'is_deleted', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('category', 'is_deleted', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('card', 'type');
        $this->dropColumn('resource', 'type');
        $this->dropColumn('category', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_112601_pseudo_delete cannot be reverted.\n";

        return false;
    }
    */
}
