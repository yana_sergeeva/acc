<?php

use yii\db\Migration;

class m160627_170421_add_account extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('account', [
            'id'           => $this->primaryKey(),
            'owner_id' => $this->integer()->notNull(),
            'default_currency_id' => $this->integer()->defaultValue(1),
            'status' => $this->integer()->notNull()->defaultValue(1),
                ], $tableOptions);

        $this->createTable('user_account', [
            'id'           => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'role' => $this->string()->defaultValue('admin'),
            'status' => $this->integer()->notNull()->defaultValue(1),
                ], $tableOptions);
    }

    public function down()
    {
        echo "m160627_170421_add_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
