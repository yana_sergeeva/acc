<?php

use yii\db\Migration;

class m170905_081018_add_default_currencies extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `currency` (`id`, `iso_code`, `symbol`, `unicode`, `position`, `comments`, `status`) VALUES
            (1, 'USD', '$', '&#36;', 'before', '', 1),
            (2, 'EUR', '€', '&#128;', 'before', '', 1),
            (3, 'CAD', '$', '&#36;', 'before', '', 1),
            (4, 'GBP', '£', '&#163;', 'before', '', 1),
            (5, 'AUD', '$', '&#36;', 'before', '', 0),
            (6, 'AED', 'د.إ', '', 'after', 'Can not find', 0),
            (7, 'ANG', 'ƒ', '&#x0192;', 'before', '', 0),
            (8, 'AOA', 'AOA', 'AOA', 'before', '', 0),
            (9, 'ARS', '$', '&#36;', 'before', '', 0),
            (10, 'BAM', 'KM', 'KM', 'before', '', 0),
            (11, 'BBD', '$', '&#36;', 'before', '', 0),
            (12, 'BGL', 'лв', '&#1083;&#1074;', 'before', 'ISO code is actually BGN', 0),
            (13, 'BHD', 'BD', 'BD', 'after', '', 0),
            (14, 'BND', '$', '&#36;', 'before', '', 0),
            (15, 'BRL', 'R$', 'R&#36;', 'before', '', 1),
            (16, 'CHF', 'Fr', 'Fr', 'before', '', 0),
            (17, 'CLF', 'UF', 'UF', 'after', '', 0),
            (18, 'CLP', '$', '&#36;', 'before', '', 0),
            (19, 'CNY', '¥', '&#165;', 'before', '', 0),
            (20, 'COP', '$', '&#36;', 'before', '', 0),
            (21, 'CRC', '₡', '&#x20A1;', 'before', '', 0),
            (22, 'CZK', 'Kč', 'K&#269;', 'after', '', 0),
            (23, 'DKK', 'kr', 'kr', 'before', '', 0),
            (24, 'EEK', 'KR', 'KR', 'before', '', 0),
            (25, 'EGP', 'E£', 'E&#163;', 'before', '', 0),
            (26, 'FJD', 'FJ$', 'FJ&#36;', 'before', '', 0),
            (27, 'GTQ', 'Q', 'Q', 'before', '', 0),
            (28, 'HKD', '$', '&#36;', 'before', '', 0),
            (29, 'HRK', 'kn', 'kn', 'before', '', 0),
            (30, 'HUF', 'Ft', 'Ft', 'before', '', 0),
            (31, 'IDR', 'Rp', 'Rp', 'before', '', 0),
            (32, 'ILS', '₪', '&#x20AA;', 'before', '', 0),
            (33, 'INR', 'Rs', 'Rs', 'before', 'There is not a unicode', 0),
            (34, 'JOD', 'د.ا', '', '', 'Can not find', 0),
            (35, 'JPY', '¥', '&#165;', 'before', '', 0),
            (36, 'KES', 'KSh', 'Ksh', 'before', '', 0),
            (37, 'KRW', '₩', '&#x20A9;', 'before', '', 0),
            (38, 'KWD', 'KD', 'KD', 'after', '', 0),
            (39, 'KYD', '$', '&#36;', 'before', '', 0),
            (40, 'LTL', 'Lt', 'Lt', 'before', '', 0),
            (41, 'LVL', 'Ls', 'Ls', 'before', '', 0),
            (42, 'MAD', 'د.م.', '', '', 'Can not find', 0),
            (43, 'MVR', 'Rf', 'Rf', 'before', '', 0),
            (44, 'MXN', '$', '&#36;', 'before', '', 0),
            (45, 'MYR', 'RM', 'RM', 'before', '', 0),
            (46, 'NGN', '₦', '&#x20A6;', 'before', '', 0),
            (47, 'NOK', 'kr', 'kr', 'before', '', 0),
            (48, 'NZD', '$', '&#36;', 'before', '', 0),
            (49, 'OMR', 'OMR', '&#65020;', 'after', 'Unsure if this is correct', 0),
            (50, 'PEN', 'S/.', 'S/.', 'before', '', 0),
            (51, 'PHP', '₱', '&#x20B1;', 'before', '', 0),
            (52, 'PLN', 'zł', 'Z&#322;', 'before', '', 0),
            (53, 'QAR', 'QAR', '&#65020;', 'after', 'Unsure if this is correct', 0),
            (54, 'RON', 'lei', 'lei', 'before', '', 0),
            (55, 'RUB', 'руб.', '&#1088;&#1091;&#1073;', 'before', '', 1),
            (56, 'SAR', 'SAR', '&#65020;', 'after', 'Unsure if this is correct', 0),
            (57, 'SEK', 'kr', 'kr', 'before', '', 0),
            (58, 'SGD', '$', '&#36;', 'before', '', 0),
            (59, 'THB', '฿', '&#322;', 'before', '', 0),
            (60, 'TRY', 'TL', 'TL', 'before', 'There is not a unicode', 0),
            (61, 'TTD', '$', '&#36;', 'before', '', 0),
            (62, 'TWD', '$', '&#36;', 'before', '', 0),
            (63, 'UAH', '₴', '&#8372;', 'before', '', 1),
            (64, 'VEF', 'Bs F', 'Bs.', 'before', '', 0),
            (65, 'VND', '₫', '&#x20AB;', 'before', '', 0),
            (66, 'XCD', '$', '&#36;', 'before', '', 0),
            (67, 'ZAR', 'R', 'R', 'before', '', 0);
        ");
    }

    public function safeDown()
    {
        echo "m160716_141644_add_currency_to_operation cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170905_081018_add_default_currencies cannot be reverted.\n";

        return false;
    }
    */
}
