<?php

use yii\db\Migration;

/**
 * Class m171208_090145_card_type
 */
class m171208_090145_card_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('card', 'type', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('card', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171208_090145_card_type cannot be reverted.\n";

        return false;
    }
    */
}
