<?php

use yii\db\Migration;

/**
 * Handles the creation for table `resource`.
 */
class m160628_163529_create_resource extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('resource', [
            'id'           => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('resource');
    }
}
