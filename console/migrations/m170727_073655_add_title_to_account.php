<?php

use yii\db\Migration;

class m170727_073655_add_title_to_account extends Migration
{
    public function safeUp()
    {
        $this->addColumn('account', 'title', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('account', 'title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170727_073655_add_title_to_account cannot be reverted.\n";

        return false;
    }
    */
}
