<?php

use yii\db\Migration;

/**
 * Handles the creation for table `card`.
 */
class m160627_171745_create_card extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('currency', [
            'id'           => $this->primaryKey(),
            'iso_code' => $this->string()->notNull(),
            'symbol' => $this->string(),
            'unicode' => $this->string(),
            'position' => $this->string(),
            'comments' => $this->string(),
            'status' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('card', [
            'id'           => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'total' => $this->float()->defaultValue(0),
            'updated_at' => $this->timestamp(),
            'display' => $this->integer()->defaultValue(1),
            'status' => $this->integer()->notNull()->defaultValue(1),
        ], $tableOptions);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('card');
    }
}
