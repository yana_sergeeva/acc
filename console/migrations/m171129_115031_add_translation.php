<?php

use yii\db\Migration;

/**
 * Class m171129_115031_add_translation
 */
class m171129_115031_add_translation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('language_source', [
            'id' => 1,
            'category' => 'message',
            'message' => "Account already exists or some error occured. If your account isn't confirmed please contact us!"
        ]);

        $this->insert('language_translate', [
            'id' => 1,
            'language' => 'ru-RU',
            'translation' => "Аккаунт уже существует либо произошла ошибка. Если ваш аккаунт не потвержден, то обратитесь в техподержку",
        ]);

        $this->insert('language_source', [
            'id' => 2,
            'category' => 'message',
            'message' => "The confirmation link is invalid or expired."
        ]);

        $this->insert('language_translate', [
            'id' => 2,
            'language' => 'ru-RU',
            'translation' => "Ссылка для потверждения неправильная или устарела. ",
        ]);

        $this->insert('language_source', [
            'id' => 3,
            'category' => 'message',
            'message' => "Please try requesting a new one."
        ]);

        $this->insert('language_translate', [
            'id' => 3,
            'language' => 'ru-RU',
            'translation' => "Попробуйте запросить новую ссылку.",
        ]);


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171129_115031_add_translation cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171129_115031_add_translation cannot be reverted.\n";

        return false;
    }
    */
}
