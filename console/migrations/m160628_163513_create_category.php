<?php

use yii\db\Migration;

/**
 * Handles the creation for table `category`.
 */
class m160628_163513_create_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('category', [
            'id'           => $this->primaryKey(),
            'icon' => $this->string(),
            'title' => $this->string()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
