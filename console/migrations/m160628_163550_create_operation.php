<?php

use yii\db\Migration;

/**
 * Handles the creation for table `operation`.
 */
class m160628_163550_create_operation extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('operation', [
            'id'           => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'from_type' => $this->string()->notNull(),
            'from_id' => $this->integer()->notNull(),
            'to_type' => $this->string()->notNull(),
            'to_id' => $this->integer()->notNull(),
            'operation' => $this->string()->notNull()->defaultValue('out'), // in/out/btw
            'amount' => $this->float()->defaultValue('0'),
            'comments' => $this->string(),
            'user_id' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp(),
            'status' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operation');
    }
}
