<?php

use yii\db\Migration;

class m160716_141644_add_currency_to_operation extends Migration
{
    public function up()
    {
        $this->addColumn('operation' , 'currency_id' , $this->integer()->defaultValue(63));
        return true;
    }

    public function down()
    {
        echo "m160716_141644_add_currency_to_operation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
