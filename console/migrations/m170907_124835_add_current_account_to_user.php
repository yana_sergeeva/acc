<?php

use yii\db\Migration;

class m170907_124835_add_current_account_to_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'current_account_id', $this->integer());

        $this->addForeignKey(
            'fk-user-current_account_id',
            'user',
            'current_account_id',
            'account',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-user-current_account_id',
            'user',
            'current_account_id'
        );
    }

    public function safeDown()
    {
        $this->dropIndex('idx-user-current_account_id','user');
        $this->dropForeignKey('fk-user-current_account_id','user');
        $this->dropColumn('user', 'current_account_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170907_124835_add_current_account_to_user cannot be reverted.\n";

        return false;
    }
    */
}
