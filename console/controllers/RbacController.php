<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $user = $auth->createRole('user');
        $auth->add($user);

        $owner = $auth->createRole('owner');
        $auth->add($owner);

        $admin = $auth->createRole('admin');
        $auth->add($admin);

    }
}